// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

//let csettings : [CSetting] = []
let package = Package(
    name: "PostgreSC",
    platforms: [
        .macOS(.v10_14),
    ],
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "PostgreSC",
            targets: ["PostgreSC"]),
    ],
    dependencies: [
        //.package(path: "../PGLib"),
        .package(url: "https://bitbucket.org/peakman/pglib.git", from: "1.0.6"),
        //.package(url: "https://github.com/vapor-community/cpostgresql.git" , from: "2.1.0"),
        //.package(path: "../Utilities"),
        .package(url: "https://bitbucket.org/peakman/Utilities.git", from: "1.0.1")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "PostgreSC",
            dependencies: ["Utilities", "PGLib"]
            ),
        .testTarget(
            name: "PostgreSCTests",
            dependencies: ["PostgreSC"]),
        .testTarget(
            name: "MetadataTests",
            dependencies: ["PostgreSC"]),
        .testTarget(
            name: "DecoderTests",
            dependencies: ["PostgreSC"]),
        .testTarget(
            name: "EncoderTests",
            dependencies: ["PostgreSC", "Utilities"]),
        .testTarget(
            name: "GenerationTests",
            dependencies: [ "PostgreSC"])
    ]
)
