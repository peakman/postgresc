//
//  PGEncoderTests.swift
//  PostgreSQLTests
//
//  Created by Steve Clarke on 28/02/2019.
//  Copyright © 2019 Steve Clarke. All rights reserved.
//

import XCTest
@testable import PostgreSC
import Utilities

let dbname: String = "hello"
let dbType: String = "hellodev"
let defaultTable = "spec"

func makeDateFormatter() -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    return dateFormatter
}
let dateFormatter = makeDateFormatter()
func makeTimestampFormatter() -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    return dateFormatter
}
let timestampFormatter = makeTimestampFormatter()


func initSpec(spec: String, table: String = defaultTable) -> Connection {
    let connection = Database.connect(dbType: dbType)
    _ = connection.execute(query: Query("drop table if exists \(table);"))
    _ = connection.execute(query: Query(spec))
    return connection
}

class PGTestSimpleInsertOfInvalidInt32: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "key  int primary key generated always as identity," +
            "i16 smallint not null" +
    ");"
    
    final class TestRow : PGEncodable {
        let key : Int? = nil
        let i16 : Int32 = Int32(1073569)
    }
    var encoder : PGEncoder<TestRow>! = nil
    let testRow = TestRow()
    var newKey : Int32? = nil
    var newInt : Int? = nil
    var saved2 : Int32? = nil
    
    override func setUp() {
        // Comment out the following row to cause a fatalError for investigation
        guard false else {recordFailure(withDescription: "Fatal error bypassed", inFile: #file, atLine: #line, expected: true) ; return}
        let conn = initSpec(spec: createSpec)
        encoder = PGEncoder<TestRow>(dbname: dbname,table: defaultTable)
        newInt = testRow.insert(on: conn, using: encoder)
        if newKey != nil {
            newInt = Int(exactly: newKey!)
        }
        let qr = conn.execute(query: "select  i16 from spec")
        saved2 = qr.readResultRowAtIndex(rowIndex: 0).columnValues[0] as? Int32
    }
    
    func testReturnedValue() {
        if encoder != nil {
            XCTAssertNotNil(newInt)
            XCTAssertEqual(newInt, 1)
            XCTAssertEqual(saved2, Int32(exactly: testRow.i16))
        }
    }
}

class PGTestSimpleInsertOfInvalidUInt: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "key  int primary key generated always as identity," +
            "u int not null" +
    ");"
    
    final class TestRow : PGEncodable {
        let key : Int? = nil
        let u : UInt = UInt(Int32.max) + 1
    }
    var encoder : PGEncoder<TestRow>! = nil
    let testRow = TestRow()
    var newKey : Int32? = nil
    var newInt : Int? = nil
    var newUInt : UInt? = nil
    
    override func setUp() {
        // Comment out the following row to cause a fatalError for investigation
        guard false else {recordFailure(withDescription: "Fatal error bypassed", inFile: #file, atLine: #line, expected: true) ; return}
        let conn = initSpec(spec: createSpec)
        encoder = PGEncoder<TestRow>(dbname: dbname,table: defaultTable)
        newKey = testRow.insert(on: conn, using: encoder)
        if newKey != nil {
            newInt = Int(exactly: newKey!)
        }
        let qr = conn.execute(query: "select u from spec")
        newUInt = UInt(exactly: qr.readResultRowAtIndex(rowIndex: 0).columnValues[0] as! Int32)
    }
    
    func testReturnedValue() {
        if encoder != nil {
            XCTAssertNotNil(newInt)
            XCTAssertEqual(newInt, 1)
            XCTAssertEqual(newUInt, testRow.u)
        }
    }
}
class PGTestSimpleInsertOfDate: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "key  int primary key generated always as identity," +
            "d date not null" +
    ");"
    
    final class TestRow : PGEncodable {
        let key : Int? = nil
        let d : Date = dateFormatter.date(from: "1946-10-28")!
    }
    var encoder : PGEncoder<TestRow>! = nil
    let testRow = TestRow()
    var newKey : Int32? = nil
    var newInt : Int? = nil
    var newDate : Date? = nil
    
    override func setUp() {
        let conn = initSpec(spec: createSpec)
        encoder = PGEncoder<TestRow>(dbname: dbname,table: defaultTable)
        newKey = testRow.insert(on: conn, using: encoder)
        if newKey != nil {
            newInt = Int(exactly: newKey!)
        }
        let qr = conn.execute(query: "select d from spec")
        newDate = qr.readResultRowAtIndex(rowIndex: 0).columnValues[0] as? Date
    }
    
    func testReturnedValue() {
        XCTAssertNotNil(newInt)
        XCTAssertEqual(newInt, 1)
        XCTAssertEqual(newDate, testRow.d)
    }
}
class PGTestSimpleInsertOfData: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "id int primary key generated always as identity," +
            "b bytea not null" +
    ");"
    
    final class TestRow : PGEncodable {
        let id : Int? = nil
        let b : Data = "Some string!!?".data(using: .utf8)!
    }
    var encoder : PGEncoder<TestRow>! = nil
    let testRow = TestRow()
    var newKey : Int32? = nil
    var newInt : Int? = nil
    var newData : Data? = nil
    
    override func setUp() {
        let conn = initSpec(spec: createSpec)
        encoder = PGEncoder<TestRow>(dbname: dbname,table: defaultTable)
        newKey = testRow.insert(on: conn, using: encoder)
        if newKey != nil {
            newInt = Int(exactly: newKey!)
        }
        let qr = conn.execute(query: "select b from spec")
        newData = qr.readResultRowAtIndex(rowIndex: 0).columnValues[0] as? Data
    }
    
    func testReturnedValue() {
        XCTAssertNotNil(newInt)
        XCTAssertEqual(newInt, 1)
        XCTAssertEqual(newData!, testRow.b)
    }
}
class PGTestSimpleInsertOfTimestamp: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "key  int primary key generated always as identity," +
            "t timestamp without time zone not null" +
    ");"
    
    final class TestRow : PGEncodable {
        let key : Int? = nil
        let t : Date = timestampFormatter.date(from: "2093-01-01T00:00:03Z")!
    }
    var encoder : PGEncoder<TestRow>! = nil
    let testRow = TestRow()
    var newKey : Int32? = nil
    var newInt : Int? = nil
    var newDate : Date? = nil
    
    override func setUp() {
        let conn = initSpec(spec: createSpec)
        encoder = PGEncoder<TestRow>(dbname: dbname,table: defaultTable)
        newKey = testRow.insert(on: conn, using: encoder)
        if newKey != nil {
            newInt = Int(exactly: newKey!)
        }
        let qr = conn.execute(query: "select t from spec")
        newDate = qr.readResultRowAtIndex(rowIndex: 0).columnValues[0] as? Date
    }
    
    func testReturnedValue() {
        XCTAssertNotNil(newInt)
        XCTAssertEqual(newInt, 1)
        XCTAssertEqual(newDate, testRow.t)
    }
}
class PGTestSimpleInsertOfValidUUID: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "key  int primary key generated always as identity," +
            "u uuid not null" +
    ");"
    
    final class TestRow : PGEncodable {
        let key : Int? = nil
        let u : UUID? = UUID()
    }
    var encoder : PGEncoder<TestRow>! = nil
    let testRow = TestRow()
    var newKey : Int32? = nil
    var newInt : Int? = nil
    var newUUID : UUID? = nil
    
    override func setUp() {
        let conn = initSpec(spec: createSpec)
        encoder = PGEncoder<TestRow>(dbname: dbname,table: defaultTable)
        newKey = testRow.insert(on: conn, using: encoder)
        if newKey != nil {
            newInt = Int(exactly: newKey!)
        }
        let qr = conn.execute(query: "select u from spec")
        newUUID = qr.readResultRowAtIndex(rowIndex: 0).columnValues[0] as? UUID
    }
    
    func testReturnedValue() {
        XCTAssertNotNil(newInt)
        XCTAssertEqual(newInt, 1)
        XCTAssertEqual(newUUID, testRow.u)
    }
}
class TestEncodeOfStructWithNilString : XCTestCase {
    let createSpec = """
    CREATE TABLE \(defaultTable) (
    i  int primary key  generated always as identity,
    u int ,
    i2 int not null,
    s varchar,
    i3 int
    );
    """
    struct T : PGEncodable {
        var i : Int? = nil
        var u : UInt = 34
        var s : String? = nil
        var i2 : Int? = 984
        var i3 : Int? = nil
    }
    var encoder : PGEncoder<T>! = nil
    var result : PGEncoderResult? = nil
    var ts : T = T()
    var nilColumns : [String] = []
    override func setUp() {
        do {
            nilColumns = ts.nilColumns
            _ = initSpec(spec: createSpec)
            encoder = PGEncoder<T>(dbname: dbname, table: defaultTable)
            result = try encoder.encodeQuery(ts)
        } catch {
            fatalError("Error is \(error.localizedDescription)")
        }
    }
    
    func testReturnedValue() {
        XCTAssertNotNil(result)
        guard result != nil else {return}
        print("Query is ", result!.query.string )
        XCTAssertTrue(result!.query.string.contains("(u,i2,s,i3)"))
    }
}
class TestEncodeOfStructWithNilsAutoIncKey : XCTestCase {
    let createSpec = """
    CREATE TABLE \(defaultTable) (
    i  int primary key  generated always as identity,
    u int ,
    i2 int not null,
    s varchar,
    i3 int
    );
    """
    struct T : PGEncodable {
        var i : Int? = nil
        var s : String? = "nil"
        var u : UInt = 34
        var i2 : Int? = 984
        var i3 : Int? = nil
    }
    var encoder : PGEncoder<T>! = nil
    var result : PGEncoderResult? = nil
    var ts : T = T()
    var nilColumns : [String] = []
    override func setUp() {
        do {
            nilColumns = ts.nilColumns
            _ = initSpec(spec: createSpec)
            encoder = PGEncoder<T>(dbname: dbname, table: defaultTable)
            result = try encoder.encodeQuery(ts)
        } catch {
            fatalError("Error is \(error.localizedDescription)")
        }
    }
    
    func testReturnedValue() {
        XCTAssertNotNil(result)
        guard result != nil else {return}
        print("Query is ", result!.query.string )
        XCTAssertTrue(result!.query.string.contains("(s,u,i2,i3)"))
    }
}

class TestEncodeOfStructWithNils : XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "i  int primary key," +
            "u int ," +
            "i2 int not null," +
            "i3 int " +
    ");"
    
    struct T : PGEncodable {
        var i : Int? = nil
        var u : UInt = 34
        var i2 : Int? = 984
        var i3 : Int? = nil
    }
    var encoder : PGEncoder<T>! = nil
    var result : PGEncoderResult? = nil
    var ts : T = T()
    var nilColumns : [String] = []
    override func setUp() {
        do {
            nilColumns = ts.nilColumns
            _ = initSpec(spec: createSpec)
            encoder = PGEncoder<T>(dbname: dbname, table: defaultTable)
            result = try encoder.encodeQuery(ts)
        } catch {
            fatalError("Error is \(error.localizedDescription)")
        }
    }
    func testNilColumns() {
        XCTAssertNotNil(nilColumns)
        XCTAssertTrue(nilColumns.contains("i"))
        XCTAssertTrue(nilColumns.contains("i3"))
        XCTAssertFalse(nilColumns.contains("u"))
        XCTAssertFalse(nilColumns.contains("i2"))
    }
    
    func testReturnedValue() {
        XCTAssertNotNil(result)
        guard result != nil else {return}
        print("Query is ", result!.query.string )
        XCTAssertTrue(result!.query.string.contains("(u,i2,i,i3)"))
    }
}

class PGTestSimpleInsertOfNil: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "key  int primary key generated always as identity," +
            "u int " +
    ");"
    
    final class TestRow : PGEncodable {
        let key : Int? = nil
        let u : UInt? = nil
    }
    var encoder : PGEncoder<TestRow>! = nil
    let testRow = TestRow()
    var newKey : Int32? = nil
    var newInt : Int? = nil
    var newUInt : UInt? = nil
    
    override func setUp() {
        let conn = initSpec(spec: createSpec)
        encoder = PGEncoder<TestRow>(dbname: dbname,table: defaultTable)
        newKey = testRow.insert(on: conn, using: encoder)
        guard newKey != nil else {XCTFail(); return}
        newInt = Int(exactly: newKey!)
        let qr = conn.execute(query: "select u from spec ")
        if let v = qr.readResultRowAtIndex(rowIndex: 0).columnValues[0] as? Int32 {
            newUInt = UInt(exactly: v)
        } else {
            newUInt = nil
        }
    }
    
    func testReturnedValue() {
        XCTAssertNotNil(newInt)
        XCTAssertEqual(newInt, 1)
        XCTAssertEqual(newUInt, testRow.u)
    }
}
class PGTestSimpleInsertOfValidUInt: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "key  int primary key generated always as identity," +
            "u int not null" +
    ");"
    
    final class TestRow : PGEncodable {
        let key : Int? = nil
        let u : UInt = UInt(exactly: Int32.max)!
    }
    var encoder : PGEncoder<TestRow>! = nil
    let testRow = TestRow()
    var newKey : Int32? = nil
    var newInt : Int? = nil
    var newUInt : UInt? = nil
    
    override func setUp() {
        let conn = initSpec(spec: createSpec)
        encoder = PGEncoder<TestRow>(dbname: dbname,table: defaultTable)
        newKey = testRow.insert(on: conn, using: encoder)
        if newKey != nil {
            newInt = Int(exactly: newKey!)
        }
        let qr = conn.execute(query: "select u from spec")
        newUInt = UInt(exactly: qr.readResultRowAtIndex(rowIndex: 0).columnValues[0] as! Int32)
    }
    
    func testReturnedValue() {
        XCTAssertNotNil(newInt)
        XCTAssertEqual(newInt, 1)
        XCTAssertEqual(newUInt, testRow.u)
    }
}
class PGTestSimpleInsertOfBoolFloatDouble: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "key  int primary key generated always as identity," +
            "b boolean not null," +
            "f real not null," +
            "d double precision not null" +
    ");"
    
    final class TestRow : PGEncodable {
        let key : Int? = nil
        let b : Bool = true
        let f : Float = 976.287
        let d : Double = 989328190120490124
    }
    var encoder : PGEncoder<TestRow>! = nil
    let testRow = TestRow()
    var newInt : Int? = nil
    var newBool : Bool? = nil
    var newFloat  : Float? = nil
    var newDouble : Double? = nil
    
    override func setUp() {
        let conn = initSpec(spec: createSpec)
        encoder = PGEncoder<TestRow>(dbname: dbname,table: defaultTable)
        let newI : Int  = testRow.insert(on: conn, using: encoder)
        newInt = newI
        let qr = conn.execute(query: "select b,f,d from spec")
        newBool = qr.readResultRowAtIndex(rowIndex: 0).columnValues[0] as? Bool
        newFloat = qr.readResultRowAtIndex(rowIndex: 0).columnValues[1] as? Float
        newDouble = qr.readResultRowAtIndex(rowIndex: 0).columnValues[2] as? Double
    }
    
    func testReturnedValue() {
        XCTAssertNotNil(newInt)
        XCTAssertEqual(newInt, 1)
        XCTAssertEqual(newBool, testRow.b)
        XCTAssertEqual(newFloat, testRow.f)
        XCTAssertEqual(newDouble, testRow.d)
    }
}
class PGTestSimpleInsertOfInt8ValidInt32: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "key  int primary key generated always as identity," +
            "i64 bigint not null," +
            "i16 smallint not null" +
    ");"
    
    final class TestRow : PGEncodable {
        let key : Int? = nil
        let i64 : Int8 = Int8(38)
        let i16 : Int32 = Int32(10739)
    }
    var encoder : PGEncoder<TestRow>! = nil
    let testRow = TestRow()
    var newKey : Int32? = nil
    var newInt : Int? = nil
    var saved1  : Int64? = nil
    var saved2 : Int16? = nil
    
    override func setUp() {
        let conn = initSpec(spec: createSpec)
        encoder = PGEncoder<TestRow>(dbname: dbname,table: defaultTable)
        newKey = testRow.insert(on: conn, using: encoder)
        if newKey != nil {
            newInt = Int(exactly: newKey!)
        }
        let qr = conn.execute(query: "select i64, i16 from spec")
        saved1 = qr.readResultRowAtIndex(rowIndex: 0).columnValues[0] as? Int64
        saved2 = qr.readResultRowAtIndex(rowIndex: 0).columnValues[1] as? Int16
    }
    
    func testReturnedValue() {
        XCTAssertNotNil(newInt)
        XCTAssertEqual(newInt, 1)
        XCTAssertEqual(saved1, Int64(exactly: testRow.i64))
        XCTAssertEqual(saved2, Int16(exactly: testRow.i16))
    }
}
class PGTestSimpleInsertWithAutoIncrement: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "key  int primary key generated always as identity," +
            "s1 varchar not null," +
            "s2 varchar not null" +
    ");"
    
    final class TestRow : PGEncodable {
        let key : Int? = 897
        let s1: String = "String number 1"
        let s2: String = "String number 2"
    }
    var encoder : PGEncoder<TestRow>! = nil
    let testRow = TestRow()
    var newKey : Int32? = nil
    var newInt : Int? = nil
    
    override func setUp() {
        let conn = initSpec(spec: createSpec)
        encoder = PGEncoder<TestRow>(dbname: dbname,table: defaultTable)
        newKey = testRow.insert(on: conn, using: encoder)
        if newKey != nil {
            newInt = Int(exactly: newKey!)
        }
    }
    
    func testReturnedValue() {
        XCTAssertNotNil(newInt)
        XCTAssertEqual(newInt, 1)
    }
}

class PGTestSimpleInsertWithoutAutoIncrement: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "key  int primary key," +
            "s1 varchar not null," +
            "s2 varchar not null" +
    ");"
    let key: Int  = 8965
    final class TestRow : PGEncodable {
        let key : Int? = 8965
        let s1: String = "String number 1"
        let s2: String = "String number 2"
    }
    var encoder : PGEncoder<TestRow>! = nil
    let testRow = TestRow()
    var newKey : Int32? = nil
    var newInt : Int? = nil
    
    override func setUp() {
        let conn = initSpec(spec: createSpec)
        encoder = PGEncoder<TestRow>(dbname: dbname,table: defaultTable)
        newKey = testRow.insert(on: conn, using: encoder)
        if newKey != nil {
            newInt = Int(exactly: newKey!)
        }
    }
    
    func testReturnedValue() {
        XCTAssertNotNil(newInt)
        XCTAssertEqual(newInt, key)
    }
}


