//
//  PGMetadataTests.swift
//  PGMetadataTests
//
//  Created by Steve Clarke on 01/03/2019.
//  Copyright © 2019 Steve Clarke. All rights reserved.
//

import XCTest
@testable import PostgreSC
let dbname: String = "hello"
let dbType: String = "hellodev"
let createSpec7 =
    "CREATE TABLE spec7 (" +
        "int16_column int2 not null default 99999," +
        "int32_column int4," +
        "int64_column int8 not null," +
        "text_column text," +
        "string_column varchar," +
        "single_float_column float4," +
        "double_float_column float8," +
        "boolean_column boolean," +
        "date_column date," +
        "timestamp_column timestamp," +
        "raw_byte_column bytea," +
        "uuid_column uuid," +
        "primary key(int16_column, int64_column)" +
");"
let createSpec8 =
    "CREATE TABLE spec8 (" +
        "int16_column int2 not null default 99999," +
        "int32_column int4," +
        "int64_column int8," +
        "text_column text," +
        "string_column varchar," +
        "single_float_column float4," +
        "double_float_column float8," +
        "boolean_column boolean," +
        "date_column date," +
        "timestamp_column timestamp," +
        "raw_byte_column bytea," +
        "uuid_column uuid" +
");"


class PGMetadataTests1: XCTestCase {
    // Table with a primary key
    var connection: Connection! = nil
    var meta : PGMetadata! = nil
    var setupDone = false
    let defaultString = "DEFAULT STRING"
    
    override func setUp() {
        if !setupDone {
            connection = Database.connect(dbType: dbType)
            _ = connection.execute(query: Query("DROP TABLE IF EXISTS spec7;"))
            _ = connection.execute(query: Query(createSpec7))
            _ = connection.execute(query: "INSERT INTO spec7 values(1223, 12345, 123456, 'abcde', 'xyz', 123.0, 98765.234, false,'1946-12-17','2000-12-19T16:39:57Z', E'\\\\x41 62 43 44 45 46'::bytea, '905902ff-963d-40e9-947f-cef0a963832d')")
            //meta = PGMetadata(connection: connection, dbname: dbname, table: "spec")
            meta = PGMetadata(connection: connection, dbname: dbname, table:"spec7",
                              query: "select int32_column, int16_column,int64_column, string_column from spec7",
                              notNull: [("string_column",defaultString ), ("int32_column", nil)])
            setupDone = true
        }
    }
    override func tearDown() {
        _ = connection.execute(query: "drop table if exists spec7 ")
    }
    
    func testPrimaryKey() {
        XCTAssertNotNil(meta.primaryKey)
        XCTAssertEqual(meta.primaryKey!.columns.count, 2)
        XCTAssertEqual(meta.primaryKey!.columns[0], "int16_column")
        XCTAssertEqual(meta.primaryKey!.pgTypes[0], ColumnType.Int16)
        XCTAssertEqual(meta.primaryKey!.columns[1], "int64_column")
        XCTAssertEqual(meta.primaryKey!.pgTypes[1], ColumnType.Int64)
    }
    func testAutoIncrements() {
        XCTAssertNotNil(meta.autoIncrements)
        XCTAssertFalse(meta.autoIncrements!)
    }
    func testColumnInfo() {
        XCTAssertNotNil(meta.columnInfo)
        XCTAssertEqual(meta.columnInfo.count, 4)
        XCTAssertNotNil(meta.infoDict["string_column"])
    }
    func testNotNull1() {
        let md = meta.infoDict["int16_column"]
        guard md != nil else {XCTFail(); return}
        XCTAssertEqual(md!.pgNullable,false)
    }
    func testNotNull2() {
        let md = meta.infoDict["int32_column"]
        guard md != nil else {XCTFail(); return}
        XCTAssertEqual(md!.pgNullable,true)
        XCTAssertEqual(md!.swiftNullable,false)
        XCTAssertNil(md!.swiftDefault)
    }
    func testNotNull3() {
        let md = meta.infoDict["string_column"]
        guard md != nil else {XCTFail(); return}
        XCTAssertEqual(md!.swiftNullable,false)
        guard md!.swiftDefault != nil else {XCTFail(); return}
        XCTAssertEqual(md!.swiftDefault! as? String, defaultString)
    }
}
class PGMetadataTests2: XCTestCase {
    // Table without a primary key
    var connection: Connection! = nil
    var meta : PGMetadata! = nil
    var setupDone = false
    let baseQ = Query("select t1.text_column, t2.boolean_column from spec8 as t1 join spec8 as t2 on t1.int16_column = t2.int16_column")
    
    override func setUp() {
        if !setupDone {
            connection = Database.connect(dbType: dbType)
            _ = connection.execute(query: Query("DROP TABLE IF EXISTS spec8;"))
            _ = connection.execute(query: Query(createSpec8))
            _ = connection.execute(query: "INSERT INTO spec8 values(1223, 12345, 123456, 'abcde', 'xyz', 123.0, 98765.234, false,'1946-12-17','2000-12-19T16:39:57Z', E'\\\\x41 62 43 44 45 46'::bytea, '905902ff-963d-40e9-947f-cef0a963832d')")
            meta = PGMetadata(connection: connection, dbname: dbname, table: nil, query: baseQ, notNull: nil)
            setupDone = true
        }
    }
    override func tearDown() {
        _ = connection.execute(query: "drop table if exists spec8 ")
    }

    func testPrimaryKey() {
        XCTAssertNil(meta.primaryKey)
    }
    func testAutoIncrements() {
        XCTAssertNil(meta.autoIncrements)
    }
    
}
