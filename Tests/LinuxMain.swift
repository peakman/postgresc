import XCTest

import PostgreSCTests

var tests = [XCTestCaseEntry]()
tests += PostgreSCTests.allTests()
XCTMain(tests)