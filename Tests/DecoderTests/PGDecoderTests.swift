//
//  PGDecoderTests.swift
//  PostgreSQLTests
//
//  Created by Steve Clarke on 28/02/2019.
//  Copyright © 2019 Steve Clarke. All rights reserved.
//

import XCTest
@testable import PostgreSC
let dbname: String = "hello"
let dbType: String = "hellodev"
let defaultTable = "spec"

func makeDateFormatter() -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    return dateFormatter
}
let dateFormatter = makeDateFormatter()
func makeTimestampFormatter() -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    return dateFormatter
}
let timestampFormatter = makeTimestampFormatter()

func defaultRetrievalQuery(table: String = defaultTable) -> Query {
    return Query("select * from \(table)")
}

func initSpec(spec: String, insertValues: String, parms: [Parameter] = [],table: String = defaultTable) -> Connection {
    let connection = Database.connect(dbType: dbType)
    _ = connection.execute(query: Query("drop table if exists \(table);"))
    _ = connection.execute(query: Query(spec))
    _ = connection.execute(query: Query("insert into \(table) values\(insertValues)"), parameters: parms )
    return connection
}
class PGTestRetrieveJoin: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "id  int primary key," +
            "s1 varchar not null," +
            "s2 varchar not null" +
    ");"
    
    let joinQuery = Query(  "select a.s1 as sa, b.s2 as sb from \(defaultTable) as a " +
                            "join \(defaultTable) as b " +
                            "on a.id = b.id")
    
    final class TestRow : PGDecodable {
        let sa: String
        let sb: String
    }
    var decoder : PGDecoder<TestRow>! = nil
    var testRow : TestRow? = nil
    let tests1 = "String number 1"
    let tests2 = "String number 2"

    override func setUp() {
        let conn = initSpec(            spec: createSpec,
                                        insertValues: "(234,$1, $2 ) ",
                                        parms: [tests1, tests2] )
        decoder = PGDecoder<TestRow>(   connection: conn, dbname: dbname,
                                        query: joinQuery)
        testRow = decoder.next()
        print(testRow! as Any)
    }
    override func tearDown() {}
    
    func testReturnedValue() {
        XCTAssertEqual(tests1, testRow?.sa)
        XCTAssertEqual(tests2, testRow?.sb)
    }
}


class PGTestNilDate: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "testcol Date " +
    ");"
    
    final class TestRow : PGDecodable {
        var testcol : Date?
    }
    var decoder : PGDecoder<TestRow>! = nil
    var testRow : TestRow? = nil
    let testval : Date? = nil
    
    override func setUp() {
        let conn = initSpec(            spec: createSpec,
                                        insertValues: "(null) ",
                                        parms: [] )
        decoder = PGDecoder<TestRow>(   connection: conn, dbname: dbname,
                                        table: defaultTable, query: defaultRetrievalQuery(),
                                        notNull: [])
        testRow = decoder.next()
    }
    override func tearDown() {}
    
    func testReturnedValue() {
        XCTAssertEqual(testval, testRow?.testcol)
    }
}

class PGTestNotNilTimestamp: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "testcol timestamp not null" +
    ");"
    
    final class TestRow : PGDecodable {
        var testcol : Date
    }
    var decoder : PGDecoder<TestRow>! = nil
    var testRow : TestRow? = nil
    let testval : Date = timestampFormatter.date(from: "2093-01-01T00:00:03Z")!
    
    override func setUp() {
        let conn = initSpec(    spec: createSpec,
                                insertValues: "($1::timestamp) ",
                                parms: [testval] )
        decoder = PGDecoder<TestRow>(   connection: conn, dbname: dbname,
                                        table: defaultTable, query: defaultRetrievalQuery(),
                                        notNull: [])
        testRow = decoder.next()
        print(testRow! as Any)
    }
    override func tearDown() {}
    
    func testReturnedValue() {
        XCTAssertEqual(testval, testRow?.testcol)
    }
}

class PGTestInvalidUInt: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "testcol  int" +
    ");"
    
    final class TestRow : PGDecodable {
        var testcol : UInt
    }
    var decoder : PGDecoder<TestRow>! = nil
    var testRow : TestRow? = nil
    let testval = UInt(6745)
    
    override func setUp() {
        let conn = initSpec(            spec: createSpec,
                                        insertValues: "($1::int ) ",
                                        parms: [-2] )
        decoder = PGDecoder<TestRow>(   connection: conn, dbname: dbname,
                                        table: defaultTable, query: defaultRetrievalQuery(),
                                        notNull: [])
        // Comment out the following row to cause a fatalError for investigation
        guard false else {recordFailure(withDescription: "Fatal error bypassed", inFile: #file, atLine: #line, expected: true) ; return}
        testRow = decoder.next()
        print(testRow! as Any)
    }
    override func tearDown() {}
    
    func testReturnedValue() {
        XCTAssertTrue(true) // Never get here because of fatal error
    }
}

class PGTestValidBytes: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "testcol  bytea" +
    ");"
    
    final class TestRow : PGDecodable {
        let testcol : Data = "A test string".data(using: .utf8)!
    }
    var decoder : PGDecoder<TestRow>! = nil
    var testRow : TestRow? = nil
    var testval : Data? = nil
    
    override func setUp() {
        let conn = initSpec(            spec: createSpec,
                                        insertValues: "($1::bytea ) ",
                                        parms: [TestRow().testcol] )
        decoder = PGDecoder<TestRow>(   connection: conn, dbname: dbname,
                                        table: defaultTable, query: defaultRetrievalQuery(),
                                        notNull: [])
        testRow = decoder.next()
        print("Data is ", String(bytes: testRow!.testcol, encoding: .utf8)!)
    }
    override func tearDown() {}
    
    func testReturnedValue() {
        XCTAssertEqual(TestRow().testcol, testRow?.testcol)
    }
}
class PGTestValidUInt: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "testcol  int2" +
    ");"
    
    final class TestRow : PGDecodable {
        var testcol : UInt
    }
    var decoder : PGDecoder<TestRow>! = nil
    var testRow : TestRow? = nil
    let testval = UInt(645)
    
    override func setUp() {
        let conn = initSpec(            spec: createSpec,
                                        insertValues: "($1::int2 ) ",
                                        parms: [testval] )
        decoder = PGDecoder<TestRow>(   connection: conn, dbname: dbname,
                                        table: defaultTable, query: defaultRetrievalQuery(),
                                        notNull: [])
        testRow = decoder.next()
        print(testRow! as Any)
    }
    override func tearDown() {}
    
    func testReturnedValue() {
        XCTAssertEqual(testval, testRow?.testcol)
    }
}

class PGTestIteration: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "testcol uuid not null" +
    ");"
    
    final class TestRow : PGDecodable {
        var testcol : UUID
    }
    var decoder : PGDecoder<TestRow>! = nil
    var testRow : TestRow? = nil
    var lastRow : TestRow? = nil
    let testval = UUID()
    let testval2 = UUID()
    
    override func setUp() {
        let conn = initSpec(    spec: createSpec,
                                insertValues: "($1::uuid) ",
                                parms: [testval] )
        _ = conn.execute(query: Query("insert into \(defaultTable) values($1::uuid)"), parameters: [testval2] )

        decoder = PGDecoder<TestRow>(connection: conn, dbname: dbname, table: defaultTable)
        for tr in decoder {
            print(tr as Any)
            if testRow == nil {
                testRow = tr
            }
            lastRow = tr
        }
    }
    override func tearDown() {}
    
    func testReturnedValue() {
        XCTAssertEqual(testval, testRow?.testcol)
        XCTAssertEqual(testval2, lastRow?.testcol)
    }
}
class PGTestUUID: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "testcol uuid not null" +
    ");"
    
    final class TestRow : PGDecodable {
        var testcol : UUID
    }
    var decoder : PGDecoder<TestRow>! = nil
    var testRow : TestRow? = nil
    let testval = UUID()
    
    override func setUp() {
        let conn = initSpec(    spec: createSpec,
                                insertValues: "($1::uuid) ",
                                parms: [testval] )
        decoder = PGDecoder<TestRow>(   connection: conn, dbname: dbname,table: defaultTable)
        testRow = decoder.next()
        print(testRow! as Any)
    }
    override func tearDown() {}
    
    func testReturnedValue() {
        XCTAssertEqual(testval, testRow?.testcol)
    }
}

class PGTestAppDefaultDate: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "testcol Date" +
    ");"
    
    final class TestRow : PGDecodable {
        var testcol : Date
    }
    var decoder : PGDecoder<TestRow>! = nil
    var testRow : TestRow? = nil
    let testval : Date = dateFormatter.date(from: "2015-02-09")!
    
    override func setUp() {
        let conn = initSpec(    spec: createSpec,
                                insertValues: "(null) ",
                                parms: [] )
        decoder = PGDecoder<TestRow>(   connection: conn, dbname: dbname,
                                        table: defaultTable, query: defaultRetrievalQuery(),
                                        notNull: [("testcol", testval)])
        testRow = decoder.next()
        print(testRow! as Any)
    }
    override func tearDown() {}
    
    func testReturnedValue() {
        XCTAssertEqual(testval, testRow?.testcol)
    }
}

class PGTestNotNilDate: XCTestCase {
    let createSpec =
        "CREATE TABLE \(defaultTable) (" +
            "testcol Date not null" +
    ");"
    
    final class TestRow : PGDecodable {
        var testcol : Date
    }
    var decoder : PGDecoder<TestRow>! = nil
    var testRow : TestRow? = nil
    let testval : Date = dateFormatter.date(from: "2015-02-09")!
    
    override func setUp() {
        let conn = initSpec(    spec: createSpec,
                                insertValues: "($1::date) ",
                                parms: [testval] )
        decoder = PGDecoder<TestRow>(   connection: conn, dbname: dbname,
                                        table: defaultTable, query: defaultRetrievalQuery(),
                                        notNull: [])
        testRow = decoder.next()
    }
    override func tearDown() {}
    
    func testReturnedValue() {
        XCTAssertEqual(testval, testRow?.testcol)
    }
}

class PGDecoderTests2: XCTestCase {
    let createSpec2 =
        "CREATE TABLE spec2 (" +
            "int16_column int2 not null, " +
            "int32_column int4 not null, " +
            "int64_column int4 , " +
            "string_column varchar ," +
            "boolean_column boolean not null," +
            "date_column date ," +
            "primary key(int16_column)" +
    ");"
    
    final class TestRow2 : PGDecodable {
        var int16_column : Int16
        var int32_column : Int
        var int64_column : Int?
        var string_column: String
        var boolean_column: Bool
        var date_column : Date
    }
    // Table with a primary key
    var connection: Connection! = nil
    var decoder : PGDecoder<TestRow2>! = nil
    var setupDone = false
    let defaultString = "DEFAULT STRING"
    var testRow : TestRow2? = nil
    var numberOfRows : Int? = nil
    let int16 = Int16(12345)
    let inta = Int(9092413)
    let intb : Int? = nil
    let testDateS = "2003-01-04 00:00:00"
    var testDate : Date {return Date.init(fromPgString: testDateS)}
    let testString = "I am a test string"
    let testBool = false
    
    override func setUp() {
        if !setupDone {
            connection = Database.connect(dbType: dbType)
            _ = connection.execute(query: Query("DROP TABLE IF EXISTS spec2;"))
            _ = connection.execute(query: Query(createSpec2))
            _ = connection.execute(query: "INSERT INTO spec2 values($1::smallint,$2::int4,$3::int8,$4,$5::boolean, $6::date)",parameters: [int16,inta,intb, testString,testBool, testDate])
            decoder = PGDecoder<TestRow2>(connection: connection, dbname: dbname,
                                          table:"spec2", query: "select * from spec2",
                                         notNull: [("string_column",defaultString )])
            numberOfRows = decoder.numberOfRows
            setupDone = true
        }
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        _ = connection.execute(query: Query("DROP TABLE IF EXISTS spec2;"))
    }
    
    func testNumberOfRows() {
        XCTAssertEqual(numberOfRows , 1)
    }
    
    func testNext() {
        testRow = decoder.next()
        XCTAssertNotNil(testRow)
        XCTAssertEqual(int16, testRow?.int16_column)
        XCTAssertEqual(inta, testRow?.int32_column)
        XCTAssertEqual(intb, testRow?.int64_column)
        XCTAssertEqual(testString, testRow?.string_column)
        XCTAssertEqual(testBool, testRow?.boolean_column)
        XCTAssertEqual(testDate, testRow?.date_column)
        print("Test row is: ", testRow!)
    }
}

class PGDecoderTests5: XCTestCase {
    let createSpec5 =
        "CREATE TABLE spec5 (" +
            "int_column int2 not null, " +
            "string_column varchar," +
            "boolean_column boolean not null," +
            "primary key(int_column)" +
    ");"
    
    final class TestRow5 : PGDecodable {
        static var queryMetadata: PGMetadata?
        var int_column : Int
        var string_column: String
        var boolean_column: Bool
    }
    
    // Table with a primary key
    var connection: Connection! = nil
    var decoder : PGDecoder<TestRow5>! = nil
    var setupDone = false
    let defaultString = "DEFAULT STRING"
    var testRow : TestRow5? = nil
    var numberOfRows : Int? = nil
    var testInt = Int(12345)
    var testString = "I am a test string"
    var testBool = false
    
    override func setUp() {
        if !setupDone {
            connection = Database.connect(dbType: dbType)
            _ = connection.execute(query: Query("DROP TABLE IF EXISTS spec5;"))
            _ = connection.execute(query: Query(createSpec5))
            _ = connection.execute(query: "INSERT INTO spec5 values($1::int,$2,$3::boolean)",parameters: [testInt,testString,testBool])
            decoder = PGDecoder<TestRow5>(connection: connection, dbname: dbname,
                                          table:"spec5", query: "select * from spec5",
                                         notNull: [("string_column",defaultString )])
            numberOfRows = decoder.numberOfRows
            setupDone = true
        }
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testNumberOfRows() {
        XCTAssertEqual(numberOfRows , 1)
    }
    
    func testNext() {
        testRow = decoder.next()
        XCTAssertNotNil(testRow)
        XCTAssertEqual(testInt, testRow?.int_column)
        XCTAssertEqual(testString, testRow?.string_column)
        XCTAssertEqual(testBool, testRow?.boolean_column)
    }
}
