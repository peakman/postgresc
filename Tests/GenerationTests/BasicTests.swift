//
//  PGMetadataTests.swift
//  PGMetadataTests
//
//  Created by Steve Clarke on 01/03/2019.
//  Copyright © 2019 Steve Clarke. All rights reserved.
//

import XCTest
@testable import PostgreSC

let dbname: String = "hello"
let dbType: String = "hellodev"
let defaultTable : String = "spec"


func makeDateFormatter() -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    return dateFormatter
}
let dateFormatter = makeDateFormatter()
func makeTimestampFormatter() -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    return dateFormatter
}
let timestampFormatter = makeTimestampFormatter()


func initSpec(spec: String, table: String = defaultTable) -> Connection {
    let connection = Database.connect(dbType: dbType)
    _ = connection.execute(query: Query("drop table if exists \(table);"))
    _ = connection.execute(query: Query(spec))
    return connection
}

class GenTest2: XCTestCase {
    // Table with a primary key
    var connection: Connection! = nil
    var meta : PGMetadata? = nil
    let defaultString = "DEFAULT STRING"
    let table = "gen2"
    var source : [String]? = nil
    
    override func setUp() {
        let createSpec = """
        CREATE TABLE \(table) (
        int16_column int2 not null default 99999,
        int32_column int4,
        int64_column int8 not null,
        text_column text,
        string_column varchar,
        single_float_column float4,
        double_float_column float8,
        boolean_column boolean,
        date_column date,
        timestamp_column timestamp,
        raw_byte_column bytea,
        uuid_column uuid,
        primary key(int16_column, int64_column)
        );
        """
        connection = initSpec(spec: createSpec, table: table)
        _ = connection.execute( query: Query("INSERT INTO \(table) values($1::smallint,$2::int,$3::bigint,$4,$5,$6::real,$7::double precision,$8::boolean,$9::date,$10::timestamp without time zone,$11::bytea,$12::uuid)"),
                                parameters: [1223, 12345, 123456, "abcde", "xyz", 123.0, 98765.234, false,"1946-12-17","2000-12-19T16:39:57Z", "\\x41 62 43 44 45 46", "905902ff-963d-40e9-947f-cef0a963832d"])
        //meta = PGMetadata(connection: connection, dbname: dbname, table: "spec")
        meta = PGMetadata(connection: connection, dbname: dbname, table: table,
                          query: Query("select int32_column, int16_column,int64_column, string_column from \(table)"),
                          notNull: [("string_column",defaultString ), ("int32_column", nil)])
        guard meta != nil else {
            recordFailure(withDescription: "Unable to create metadata", inFile: #file, atLine: #line, expected: false) ; return
        }
        source =  meta!.generateModel(modelType: "BigModel")
        guard source != nil else {
            recordFailure(withDescription: "Unable to createsource", inFile: #file, atLine: #line, expected: false) ; return
        }
    }
    
    func testGenFromMeta() {
        XCTAssertNotNil(source)
    }
}

