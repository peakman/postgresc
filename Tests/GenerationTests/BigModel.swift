import Foundation
import PostgreSC
public final class BigModel : PGEncodable, PGDecodable {
	public let int32_column : Int
	public let string_column : String
	public init(int32_column: Int, string_column: String ) {
		self.int32_column = int32_column
		self.string_column = string_column
	}
}
