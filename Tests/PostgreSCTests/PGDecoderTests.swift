//
//  PGDecoderTests.swift
//  PostgreSQLTests
//
//  Created by Steve Clarke on 28/02/2019.
//  Copyright © 2019 Steve Clarke. All rights reserved.
//

import XCTest
@testable import PostgreSC
let dbname: String = "hello"
let dbType: String = "hellodev"
let createSpec1 =
    "CREATE TABLE spec1 (" +
        "int16_column int2 not null, " +
        "string_column varchar," +
        "boolean_column boolean not null," +
        "primary key(int16_column)" +
");"

final class TestRow : PGDecodable {
    var int16_column : Int16
    var string_column: String
    var boolean_column: Bool
}


class PGDecoderTests1: XCTestCase {
    // Table with a primary key
    var connection: Connection! = nil
    var decoder : PGDecoder<TestRow>! = nil
    var setupDone = false
    let defaultString = "DEFAULT STRING"
    var testRow : TestRow? = nil
    var numberOfRows : Int? = nil
    var int16 = Int16(12345)
    var testString = "I am a test string"
    var testBool = false
    
    override func setUp() {
        if !setupDone {
            connection = Database.connect(dbType: dbType)
            _ = connection.execute(query: Query("DROP TABLE IF EXISTS spec1;"))
            _ = connection.execute(query: Query(createSpec1))
            _ = connection.execute(query: "INSERT INTO spec1 values($1::smallint,$2,$3::boolean)",parameters: [int16,testString,testBool])
            decoder = PGDecoder<TestRow>(connection: connection, dbname: dbname,
                                         table:"spec1", query: "select * from spec1",
                                         notNull: [("string_column",defaultString )])
            numberOfRows = decoder.numberOfRows
            setupDone = true
        }
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        _ = connection.execute(query: "drop table if exists spec1 ")
    }
    
    func testNumberOfRows() {
        XCTAssertEqual(numberOfRows , 1)
    }
    
    func testNext() {
        testRow = decoder.next()
        XCTAssertNotNil(testRow)
        XCTAssertEqual(int16, testRow?.int16_column)
        XCTAssertEqual(testString, testRow?.string_column)
        XCTAssertEqual(testBool, testRow?.boolean_column)
    }
}
let createSpec2 =
    "CREATE TABLE spec2 (" +
        "int16_column int2 not null, " +
        "string_column varchar," +
        "boolean_column boolean not null," +
        "primary key(int16_column)" +
");"

final class TestRow2 : PGDecodable {
    static var queryMetadata: PGMetadata?
    var int16_column : Int
    var string_column: String
    var boolean_column: Bool
}


class PGDecoderTests2: XCTestCase {
    // Table with a primary key
    var connection: Connection! = nil
    var decoder : PGDecoder<TestRow2>! = nil
    var setupDone = false
    let defaultString = "DEFAULT STRING"
    var testRow : TestRow2? = nil
    var numberOfRows : Int? = nil
    var testInt = Int(12345)
    var testString = "I am a test string"
    var testBool = false
    
    override func setUp() {
        if !setupDone {
            connection = Database.connect(dbType: dbType)
            _ = connection.execute(query: Query("DROP TABLE IF EXISTS spec2;"))
            _ = connection.execute(query: Query(createSpec2))
            _ = connection.execute(query: "INSERT INTO spec2 values($1::int,$2,$3::boolean)",parameters: [testInt,testString,testBool])
            decoder = PGDecoder<TestRow2>(connection: connection, dbname: dbname,
                                          table:"spec2", query: "select * from spec2",
                                         notNull: [("string_column",defaultString )])
            numberOfRows = decoder.numberOfRows
            setupDone = true
        }
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        _ = connection.execute(query: "drop table if exists spec2 ")
    }
    
    func testNumberOfRows() {
        XCTAssertEqual(numberOfRows , 1)
    }
    
    func testNext() {
        testRow = decoder.next()
        XCTAssertNotNil(testRow)
        XCTAssertEqual(testInt, testRow?.int16_column)
        XCTAssertEqual(testString, testRow?.string_column)
        XCTAssertEqual(testBool, testRow?.boolean_column)
    }
}
