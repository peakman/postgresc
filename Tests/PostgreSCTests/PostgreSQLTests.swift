//
//  postgres_fwTests.swift
//  postgres_fwTests
//
//  Created by Steve Clarke on 08/01/2018.
//  Copyright © 2018 Steve Clarke. All rights reserved.
//

import XCTest
@testable import PostgreSC


func reportError(_ result: QueryResult) {
    debugPrint("Error message: \(result.errorMessage)")
}

public enum Columns : String {
    case int16_column, int32_column
}

class BasicRetrieval: XCTestCase {
    
    var connection: Connection!
    var connectionErrorMessage: String?
    static var dateFormatter : DateFormatter? = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    static var timestampFormatter : DateFormatter? = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        return dateFormatter
    }()

    override func setUp() {
        super.setUp()
        connection = Database.connect()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSmallint() {
        let result = connection.execute(query: "SELECT CAST (8765 as smallint) ")
        XCTAssertEqual(result.numberOfRows, 1)
        let row = result.rows[0]
        debugPrint("Value is \(String(describing: row["int2"]))")
        guard let i = row["int2"] as? Int16 else { XCTFail(); return}
        XCTAssertEqual(i ,8765)
    }

    func testUUID() {
        let uuid = UUID()
        print("uuid is \(uuid)")
        let qs = Query("SELECT CAST ('\(uuid)' as uuid) ")
        let result = connection.execute(query: qs)
        XCTAssertEqual(result.numberOfRows, 1)
        let row = result.rows[0]
        let uuidr  = row["uuid"]! as! UUID
        XCTAssertEqual(uuidr, uuid)
    }

    func testInteger() {
        let result = connection.execute(query: "SELECT CAST (8765 as integer) ")
        XCTAssertEqual(result.numberOfRows, 1)
        let row = result.rows[0]
        debugPrint("Value is \(String(describing: row["int4"]))")
        guard let i = row["int4"] as? Int32 else { XCTFail(); return}
        XCTAssertEqual(i ,8765)
    }

    func testReal() {
        let result =  connection.execute(query: "SELECT CAST ( 87659.23 as real)")
        XCTAssertEqual(result.numberOfRows, 1)
        let row = result.rows[0]
        debugPrint("Value is \(String(describing: row["float4"]))")
        guard let f = row["float4"] as? Float else {XCTFail(); return}
        XCTAssertEqual(f ,87659.23)
    }
    
    func testDouble() {
        let result =  connection.execute(query: "SELECT  8887659.23::double precision")
        XCTAssertEqual(result.numberOfRows, 1)
        let row = result.rows[0]
        debugPrint("Value is \(String(describing: row["float8"]))")
        guard let f = row["float8"] as? Float64 else {XCTFail(); return}
        XCTAssertEqual(f ,8887659.23)
    }
    
    func testString() {
        let result =  connection.execute(query: "SELECT 'abcde' as varchar")
        XCTAssertEqual(result.numberOfRows, 1)
        let row = result.rows[0]
        debugPrint("Value is \(String(describing: row["varchar"]))")
        guard let s = row["varchar"] as? String else {XCTFail(); return}
        XCTAssertEqual(s, "abcde")
    }
    
    func testText() {
        let result =  connection.execute(query: "SELECT 'abcde' as text")
        XCTAssertEqual(result.numberOfRows, 1)
        let row = result.rows[0]
        debugPrint("Value is \(String(describing: row["text"]))")
        guard let s = row["text"] as? String else {XCTFail(); return}
        XCTAssertEqual(s, "abcde")
    }
    
    func testBoolean() {
        let result =  connection.execute(query: "SELECT true as boolean")
        XCTAssertEqual(result.numberOfRows, 1)
        let row = result.rows[0]
        debugPrint("Value is \(String(describing: row["boolean"]))")
        guard let b = row["boolean"] as? Bool else {XCTFail(); return}
        XCTAssertTrue(b)
    }
    
    func testDate1() {
        let dateString = "2003-01-04"
        let qury : Query = Query("SELECT  '\(dateString)'::date as date")
        debugPrint("query: \(qury)")
        let result = connection.execute(query: qury, parameters: [])
        XCTAssertEqual(result.numberOfRows, 1)
        let row = result.rows[0]
        guard let d = row["date"] as? Date else {XCTFail(); return}
        debugPrint("Value is \(String(describing: row["date"] as! Date))")
        XCTAssertEqual(d,BasicRetrieval.dateFormatter!.date(from: dateString))
    }
    
    func testDate2() {
        let dateString = "1999-01-04"
        let qury : Query = Query("SELECT  '\(dateString)'::date as date")
        debugPrint("query: \(qury)")
        let result = connection.execute(query: qury, parameters: [])
        XCTAssertEqual(result.numberOfRows, 1)
        let row = result.rows[0]
        guard let d = row["date"] as? Date else {XCTFail(); return}
        debugPrint("Value is \(String(describing: row["date"] as! Date))")
        XCTAssertEqual(d,BasicRetrieval.dateFormatter!.date(from: dateString))
    }
    
    func testTimestamp() {
        //let dateString = "2000-12-19T16:39:57Z"
        let dateString = "2093-01-01T00:00:03Z"
        let qury : Query = Query("SELECT '\(dateString)'::timestamp as timestamp")
        let testDate = BasicRetrieval.timestampFormatter!.date(from: dateString)
        debugPrint("query: \(qury)")
        let result = connection.execute(query: qury, parameters: [])
        XCTAssertEqual(result.numberOfRows, 1)
        let row = result.rows[0]
        guard let d = row["timestamp"] as? Date else {XCTFail(); return}
        debugPrint("Value is \(String(describing: row["timestamp"] as! Date))")
        debugPrint("Exepcted offset (seconds)\( testDate!.timeIntervalSince(result.timestampReferenceDate!))")
        XCTAssertEqual(d,BasicRetrieval.timestampFormatter!.date(from: dateString))
    }

}


class DBRetrieval1: XCTestCase {
    
    static var dateFormatter : DateFormatter? = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    static var timestampFormatter : DateFormatter? = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        return dateFormatter
    }()
    var connection: Connection!
    var connectionErrorMessage: String?
    var result1: QueryResult!
    var row: QueryResultRow?
    
    override func setUp() {
        super.setUp()
        connection = Database.connect()
        let createTable1 =
            "CREATE TABLE spec1 (" +
                "int16_column int2," +
                "int32_column int4," +
                "int64_column int8," +
                "text_column text," +
                "string_column varchar," +
                "single_float_column float4," +
                "double_float_column float8," +
                "boolean_column boolean," +
                "date_column date," +
                "timestamp_column timestamp," +
                "raw_byte_column bytea," +
                "uuid_column uuid" +
        ");"
        
        _ = connection.execute(query: Query("DROP TABLE IF EXISTS spec1;"))
        _ = connection.execute(query: Query(createTable1))
        _ = connection.execute(query: "INSERT INTO spec1 values(1223, 12345, 123456, 'abcde', 'xyz', 123.0, 98765.234, false,'1946-12-17','2000-12-19T16:39:57Z', E'\\\\x41 62 43 44 45 46'::bytea, '905902ff-963d-40e9-947f-cef0a963832d')")
        result1 = connection.execute(query: "SELECT * FROM spec1 ")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        _ = connection.execute(query: "drop table if exists spec1 ")
    }
    
    func testZeroRowsReturned() {
        let result0 = connection.execute(query: "SELECT int16_column FROM spec1 where int16_column is null")
        debugPrint("There are \(result0.numberOfRows) rows returned")
        XCTAssertEqual(result0.numberOfRows, 0)
    }
    
    func testBytes() {
        let bytes  = result1.rows[0]["raw_byte_column"]!
        XCTAssertEqual(String(data: bytes as! Data, encoding: String.Encoding.utf8 )!, "AbCDEF")
    }
    
    func testInt16() {
        XCTAssertEqual(result1.rows[0]["int16_column"]!  as! Int16, 1223)
    }
    
    func testInt32() {
        XCTAssertEqual(result1.rows[0]["int32_column"]!  as! Int32, 12345)
    }
    
    func testInt64() {
        XCTAssertEqual(result1.rows[0]["int64_column"]!  as! Int64, 123456)
    }
    
    func testText() {
        XCTAssertEqual(result1.rows[0]["text_column"]!  as! String, "abcde")
    }
    
    func testString() {
        XCTAssertEqual(result1.rows[0]["string_column"]!  as! String, "xyz")
    }
    
    func testFloat() {
        XCTAssertEqual(result1.rows[0]["single_float_column"]!  as! Float, 123.0)
    }
    
    func testDouble() {
        XCTAssertEqual(result1.rows[0]["double_float_column"]!  as! Double, 98765.234)
    }
    
    func testBool() {
        XCTAssertEqual(result1.rows[0]["boolean_column"]!  as! Bool, false)
    }
    
    func testDate() {
        XCTAssertEqual(result1.rows[0]["date_column"]!  as? Date, DBRetrieval1.dateFormatter?.date(from: "1946-12-17")!)
    }
    
    func testTimestamp() {
        XCTAssertEqual(result1.rows[0]["timestamp_column"]!  as? Date, DBRetrieval1.timestampFormatter?.date(from: "2000-12-19T16:39:57Z")!)
    }
    
    func testOneValidDateParameter() {
        let susieBirth : Parameter = BasicRetrieval.dateFormatter!.date(from: "1946-12-17")!
        let res = connection.execute(query: "SELECT int16_column FROM spec1 where date_column = $1::date",parameters:  [susieBirth])
        guard res.numberOfRows == 1 else {XCTFail(); return}
        guard let i16 = res.rows[0]["int16_column"] as? Int16 else {XCTFail(); return}
        XCTAssertEqual(i16  ,1223 )
    }
    
    func testOneValidBoolParameter() {
        let res =  connection.execute(query: "SELECT int16_column FROM spec1 where boolean_column = $1::boolean",parameters:  [false as Parameter])
        guard res.numberOfRows == 1 else {XCTFail(); return}
        guard let i16 = res.rows[0]["int16_column"] as? Int16 else {XCTFail(); return}
        XCTAssertEqual(i16  ,1223 )
    }
    
    func testOneValidStringParameter() {
        let res = connection.execute(query: "SELECT int16_column FROM spec1 where string_column = $1",parameters:  ["xyz"])
        guard res.numberOfRows == 1 else {XCTFail(); return}
        guard let i16 = res.rows[0]["int16_column"] as? Int16 else {XCTFail(); return}
        XCTAssertEqual(i16  ,1223 )
    }
    
    func testOneValidIntParameter() {
        let resa = connection.execute(query: "SELECT string_column FROM spec1 where int16_column = $1::int",parameters:  ["1223"])
        guard resa.numberOfRows == 1 else {XCTFail(); return}
        guard let s = resa.rows[0]["string_column"] as? String else {XCTFail(); return}
        XCTAssertEqual(s  ,"xyz" )
    }
    
    func testTwoValidStringParameters() {
        let res = connection.execute(query: "SELECT int16_column FROM spec1 where string_column = $1 and text_column = $2",parameters:  ["xyz", "abcde"])
        guard res.numberOfRows == 1 else {XCTFail(); return}
        guard let i16 = res.rows[0]["int16_column"] as? Int16 else {XCTFail(); return}
        XCTAssertEqual(i16  ,1223 )
    }

    func testInvalidStringParameter() {
        let res =  connection.execute(query: "SELECT int16_column FROM spec1 where string_column = $1",parameters:  ["xyzx"])
        XCTAssertTrue(res.numberOfRows == 0)
    }
    
    func testTwoInvalidStringParameters() {
        let res = connection.execute(query: "SELECT int16_column FROM spec1 where string_column = $1 and string_column = $2",parameters:  ["xyz", "xyz845"])
        XCTAssertTrue(res.numberOfRows == 0)
    }
    
    func testArrayIntsParameter() {
        let ids : [Any] = [1223,43]
        let res = connection.execute(query: "select string_column from spec1 where int16_column = ANY($1::int[]);", parameters: [ids ])
        guard res.numberOfRows > 0 else { XCTFail("Less than 1 row returned"); return }
        XCTAssertEqual(res.rows[0]["string_column"] as? String, "xyz")
    }
    
}

class DBUpdate9: XCTestCase {
    
    static var dateFormatter : DateFormatter? = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    static var timestampFormatter : DateFormatter? = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        return dateFormatter
    }()
    var connection: Connection!
    var connectionErrorMessage: String?
    var qresult: QueryResult?
    var row: QueryResultRow?
    
    override func setUp() {
        super.setUp()
        connection = Database.connect()
        let createTable9 =
            "CREATE TABLE spec9 (" +
                "int16_column int2," +
                "int32_column int4," +
                "int64_column int8," +
                "text_column text," +
                "string_column varchar," +
                "single_float_column float4," +
                "double_float_column float8," +
                "boolean_column boolean," +
                "date_column date," +
                "timestamp_column timestamp," +
                "raw_byte_column bytea" +
        ");"
        
        _ = connection.execute(query: Query("drop table if exists spec9;"))
        _ = connection.execute(query: Query(createTable9))
        _ = connection.execute(query: "INSERT INTO spec9 values(1223, 12345, 123456, 'abcde', 'xyz', 123.0, 98765.234, false,'1946-12-17','2000-12-19T16:39:57Z', null)")
        _ = connection.execute(query: "SELECT * FROM spec9 ")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        _ = connection.execute(query: "drop table if exists spec9 ")
    }
    func testUpdateString() {
        let newString = "updated value"
        let res =  connection.execute(query: "UPDATE spec9 SET string_column = $1",parameters:  [newString])
        XCTAssertTrue(res.numberOfRows == 0)
        let res2 = connection.execute(query: "SELECT string_column from spec9 ")
        XCTAssertTrue(res2.numberOfRows == 1)
        guard let s = res2.rows[0]["string_column"] as? String else {XCTFail(); return}
        XCTAssertEqual(s  ,newString )
    }
    
    func testUpdateInt32() {
        let newNum = Int32(6789)
        let res = connection.execute(query: "UPDATE spec9 SET int32_column = $1::int",parameters:  [String(newNum)])
        XCTAssertTrue(res.numberOfRows == 0)
        let res2 =  connection.execute(query: "SELECT int32_column from spec9 ")
        XCTAssertTrue(res2.numberOfRows == 1)
        guard let s = res2.rows[0]["int32_column"] as? Int32 else {XCTFail(); return}
        XCTAssertEqual(s  ,newNum )
    }
    
}






