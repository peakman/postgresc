import libpq
import AppKit

public protocol Parameter {
    func asPgString() -> String
}

extension Parameter {
    public func asPgString() -> String {return "\(self)"}
}

extension String: Parameter {
    public func asPgString() -> String {return self}
}

extension NSNull: Parameter {
    public func asPgString() -> String { return ""}
}
extension UUID {
    public init(_ uuid: UUID) {
        self = UUID(uuid: uuid.uuid)
    }
}
extension UInt : Parameter {}
extension UUID: Parameter {}
extension Int: Parameter {}
extension Int16: Parameter {}
extension Int32: Parameter {}
extension Int64: Parameter {}
extension UInt32: Parameter {}
extension Bool: Parameter {}
extension Date : Parameter {
    static var timestampFormatter : DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }()
    public func asPgString() -> String {return Date.timestampFormatter.string(from: self)}
    public init(fromPgString: String) {
        let date = Date.timestampFormatter.date(from: fromPgString)
        self.init(timeInterval: 0 , since: date! )
    }
    public init(fromPgDate: Date) {
        self.init(timeInterval: 0 , since: fromPgDate )
    }
}
extension Data : Parameter {
    public func asPgString() -> String {
        let hex = "\\x"
        let res : String = self.reduce(hex) {(mem: String, item: UInt8) in
            return mem.appending(String(format: "%02x", item))
        }
        return res
    }
}
extension Float : Parameter {}
extension Double : Parameter {}
extension Array : Parameter {
    public func asPgString() -> String {
        guard (self.reduce(true) { (mem,item) in
            switch item {
            case is Int: return true
            default: return false
            }})  else {fatalError("Array parameters can only have Int elements")}
        let valueString = self.map() {String($0 as! Int)}.joined(separator: ",")
        return "{\(valueString)}"
    }
}

