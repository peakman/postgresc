import libpq

/// A database connection is NOT thread safe.
public class Connection {
    let connectionPointer: OpaquePointer

    init(pointer: OpaquePointer) {
        self.connectionPointer = pointer
    }

    deinit {
        PQfinish(connectionPointer)
    }

    public func execute(query: Query, parameters: [Parameter?] = [])  -> QueryResult {
        var result : QueryResult
        do {
            result = try  reallyExecute(query: query, parameters: parameters.map() {$0?.asPgString()})
        } catch let pge as PgError  {
            switch pge {
            case .ConnectionFailed(message: let message):  fatalError("Connection error: \(message))")
            case .Unclassified(message: let message):  fatalError("Unclassified Pg error: \(message))")
            }
        } catch let err as QueryError  {
            fatalError("Failed on query \(query). Error is \(err.localizedDescription)")
        } catch let err2 {
            fatalError("Failed with error \(err2)")
        }
        return result
    }

    /// Executes a passed in query. First parameter is referred to as `$1` in the query.
    public func reallyExecute(query: Query, parameters: [String?] = []) throws -> QueryResult {
        //let values = UnsafeMutablePointer<UnsafePointer<Int8>?>.allocate(capacity: parameters.count)
        var values : [UnsafePointer<Int8>?]
        var queryResult : QueryResult? = nil
        var status : ExecStatusType = PGRES_FATAL_ERROR
        var resultPointer : OpaquePointer? = nil
        
        if parameters.count != 0 {
            var stringBytes = [ContiguousArray<CChar>]()
            values = [UnsafePointer<Int8>?]()
            var paramTypes = [Oid]()
            stringBytes.reserveCapacity(parameters.count)
            values.reserveCapacity( parameters.count)

            for (i,val) in parameters.enumerated() {
                paramTypes.append(ColumnType.String.rawValue)
                if let value = val {
                    stringBytes.append(value.utf8CString)
                    stringBytes[i].withUnsafeBufferPointer() { values.append( $0.baseAddress!)}
                } else {
                    stringBytes.append(ContiguousArray<CChar>())
                    values.append(UnsafePointer<Int8>(bitPattern: 0))
                }
            }
            resultPointer = PQexecParams(connectionPointer,
                                         query.string,
                                         Int32(parameters.count),
                                         paramTypes,
                                         values,
                                         nil, //lengthsPtr[0],
                                         nil, //formatsPtr[0],
                                         query.resultFormat.rawValue)
            status = PQresultStatus(resultPointer)
        } else {
            resultPointer = PQexecParams(connectionPointer,
                                             query.string,
                                             Int32(0),
                                             nil,
                                             nil,
                                             nil,
                                             nil,
                                             query.resultFormat.rawValue)
            status = PQresultStatus(resultPointer)
        }

        switch status {
        case PGRES_TUPLES_OK, PGRES_COMMAND_OK: break
        default:
            let message = String(cString: PQresultErrorMessage(resultPointer))
            //debugPrint("Error message: \(message)")
            throw QueryError.invalidQuery(errorMessage: message)
        }
        
        queryResult = QueryResult(resultPointer: resultPointer!)

        return queryResult!
    }
}

// TODO: Implement on Connection
public enum ConnectionStatus {
    case Connected
    case Disconnected
}
