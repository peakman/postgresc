import libpq
import Foundation

public struct QueryResultIterator: IteratorProtocol {
    let qr: QueryResult
    var index = 0
    
    init(_ queryResult: QueryResult) {
        self.qr = queryResult
    }
    
    public mutating func next() -> QueryResultRow? {
        guard index < qr.numberOfRows
            else { return nil }
        
        let row : QueryResultRow? = qr.readResultRowAtIndex(rowIndex: Int32(index))
        index += 1
        return row
    }
}

/// Results are readonly operations and therefore threadsafe.
public final class QueryResult : Sequence {
    public static var referenceDate: Date? = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: "2000-01-01")
    }()
    public static var timestampReferenceDate: Date? = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        return dateFormatter.date(from: "2000-01-01T00:00:00Z")
    }()
    
    public let resultPointer: OpaquePointer

    init(resultPointer: OpaquePointer) {
        self.resultPointer = resultPointer
    }

    deinit {
        PQclear(resultPointer)
    }
    
    public func makeIterator() -> QueryResultIterator {
        return QueryResultIterator(self)
    }
        
    public var timestampReferenceDate : Date?  { return QueryResult.timestampReferenceDate}
    public var referenceDate : Date?  { return QueryResult.referenceDate}

    public lazy var columnIndexesForNames: [String: Int] = {
        var columnIndexesForNames = [String: Int]()

        for columnNumber in 0..<self.numberOfColumns {
            let name = String(cString: PQfname(self.resultPointer, Int32(columnNumber)))
            columnIndexesForNames[name] = Int(columnNumber)
        }
        //debugPrint("columnIndexesForNames: \(columnIndexesForNames)")
        return columnIndexesForNames
    }()

    public lazy var numberOfRows: Int = {
        return Int(PQntuples(self.resultPointer))
    }()

    public lazy var numberOfColumns: Int = {
        return Int(PQnfields(self.resultPointer))
    }()

   public lazy var typesForColumnIndexes: [ColumnType?] = {
        var typesForColumns = [ColumnType?]()
        typesForColumns.reserveCapacity(Int(self.numberOfColumns))
    
        for columnNumber in 0..<self.numberOfColumns {
            let typeId = PQftype(self.resultPointer, Int32(columnNumber))
            if let  colType = ColumnType(rawValue: typeId) {
                typesForColumns.append(colType)
            } else {
                debugPrint("Unrecognized type id: \(typeId)")
                typesForColumns.append(nil)
            }
        }
        //debugPrint("typesForColumns: \(typesForColumns)")
        return typesForColumns
    }()

    public lazy var rows: [QueryResultRow] = {
        var rows = [QueryResultRow]()
        rows.reserveCapacity(Int(self.numberOfRows))

        for rowIndex in 0..<self.numberOfRows {
            rows.append(self.readResultRowAtIndex(rowIndex: Int32(rowIndex)))
        }

        return rows
    }()
    
    public var errorMessage: String {return String(cString: PQresultErrorMessage(resultPointer)) }

    public func readResultRowAtIndex(rowIndex: Int32) -> QueryResultRow {
        var values = [Any?]()
        values.reserveCapacity(Int(self.numberOfColumns))

        for columnIndex in 0..<Int(self.numberOfColumns) {
            let value : Any? = readColumnValueAtIndex(columnIndex: Int32(columnIndex), rowIndex: rowIndex)
            values.append(value)
        }

        return QueryResultRow(columnValues: values,  queryResult: self)
    }
        
    private func readColumnValueAtIndex(columnIndex: Int32, rowIndex: Int32) -> Any? {
        guard PQgetisnull(self.resultPointer, rowIndex, columnIndex) == 0 else { return nil }
        let length = Int(PQgetlength(self.resultPointer, rowIndex, columnIndex))

        guard let type = self.typesForColumnIndexes[Int(columnIndex)] else  {
            //let length = Int(PQgetlength(self.resultPointer, rowIndex, columnIndex))
            // Unsupported column types are returned as [UInt8]
            // return byteArrayForPointer(start: UnsafePointer<UInt8>(startingPointer!), length: length)
            return 9999 // Need to sort this out
        }
        //debugPrint("type is \(String(describing: type))")

        guard let startingPointer : UnsafeMutablePointer<Int8> = PQgetvalue(self.resultPointer, rowIndex, columnIndex) else {fatalError("stating pointer is nil")}
        var value : Any? = nil
        switch type {
        case .Int16: startingPointer.withMemoryRebound(to: Int16.self, capacity: 1) { value = $0.pointee.bigEndian}
        case .Int32: startingPointer.withMemoryRebound(to: Int32.self, capacity: 1) { value = $0.pointee.bigEndian}
        case .Int64: startingPointer.withMemoryRebound(to: Int64.self, capacity: 1) { value = $0.pointee.bigEndian}
        case .SingleFloat: startingPointer.withMemoryRebound(to: UInt32.self, capacity: 1) { value = Float32(bitPattern: $0.pointee.bigEndian)}
        case .DoubleFloat: startingPointer.withMemoryRebound(to: UInt64.self, capacity: 1) { value = Float64(bitPattern: $0.pointee.bigEndian)}
        case .String: value = String(cString: startingPointer)
        case .Bytes: let sp = UnsafeMutableRawPointer(startingPointer)
            let tval = Data(bytesNoCopy: sp,count: length,deallocator: .none)
            value = Data( tval)
        case .Text: value = String(cString: startingPointer)
        case .Boolean: startingPointer.withMemoryRebound(to: Bool.self, capacity: 1) { value = $0.pointee}
        case .UUID: let sp = UnsafeMutableRawPointer(startingPointer)
            let uuids = Data(bytesNoCopy: sp,count: length,deallocator: .none)
            let uuidt : uuid_t = (uuids[0],uuids[1],uuids[2], uuids[3], uuids[4], uuids[5], uuids[6], uuids[7], uuids[8], uuids[9], uuids[10], uuids[11], uuids[12], uuids[13],uuids[14], uuids[15])
            value = UUID(uuid: uuidt)
        case .Date:
            startingPointer.withMemoryRebound(to: UInt32.self, capacity: 1) {
                let offset = Float32(Int32(bitPattern: $0.pointee.bigEndian))
                //debugPrint(("offset: \(offset)"))
                value = QueryResult.referenceDate!.addingTimeInterval(TimeInterval(offset * 3600.0 * 24) ) }
        case .Timestamp:
            startingPointer.withMemoryRebound(to: UInt64.self, capacity: 1) {
                let offset = Double(exactly: $0.pointee.bigEndian )!  / 1000000.0
                //debugPrint("UInt64: \($0.pointee.bigEndian)")
                //debugPrint(("offset: \(offset)"))
                //debugPrint("Timestamp reference date: \(QueryResult.timestampReferenceDate!)")
                value = QueryResult.timestampReferenceDate!.addingTimeInterval(TimeInterval(offset)) }
        /*
        default:
            value = "Return value not yet available"
            debugPrint("defualt value is \(String(describing: value))")
        */
        }
        //debugPrint("value is \(String(describing: value))")
        return value
    }

    private func byteArrayForPointer(start: UnsafePointer<UInt8>, length: Int) -> [UInt8] {
        return Array(UnsafeBufferPointer(start: start, count: length))
    }
}

public enum ColumnType: UInt32 {
    case Boolean = 16
    case Int64 = 20
    case Int16 = 21
    case Int32 = 23
    case Text = 25
    case SingleFloat = 700
    case DoubleFloat = 701
    case Date = 1082
    case Timestamp = 1114
    case String = 1043
    case Bytes = 17
    case UUID = 2950
    
    static func fromPsql(string: String) -> ColumnType{
        let ct : ColumnType
        switch string {
        case "boolean": ct = .Boolean
        case "bigint": ct = .Int64
        case "smallint": ct = .Int16
        case "integer": ct = .Int32
        case "text": ct = .Text
        case "character varying","character varying(255)","character varying(100)" : ct = .String
        case "real": ct = .SingleFloat
        case "double precision": ct = .DoubleFloat
        case "date": ct = .Date
        case "timestamp without time zone": ct = .Timestamp
        case "bytea": ct = .Bytes
        case "uuid": ct = .UUID
        default: fatalError("Cannot convert string \(string) into ColumnType")
        }
        return ct
    }
}
