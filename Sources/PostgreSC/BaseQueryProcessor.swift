//
//  BaseQueryProcessor.swift
//  Postgres
//
//  Created by Steve Clarke on 01/03/2019.
//  Copyright © 2019 Steve Clarke. All rights reserved.
//

import Foundation
import libpq

public protocol BaseQueryProcessor {
    static func gatherMetadata(col: Int, qRes: QueryResult) -> MetaFromQuery
}

public typealias MetaFromQuery = (col: Int, name: String, swiftType: Parameter.Type,
    dbType: ColumnType,  asString: ((Parameter) -> String)?, fromString: ((String) -> Parameter)?)

extension BaseQueryProcessor {
    public static func gatherMetadata(col: Int, qRes: QueryResult) -> MetaFromQuery {
        guard let colType = qRes.typesForColumnIndexes[col] else  {
            fatalError("\n\nUnknown type \(String(describing: qRes.typesForColumnIndexes[col]))\nfor column \(String(cString: PQfname(qRes.resultPointer, Int32(col))))\n\n")
        }
        let name = String(cString: PQfname(qRes.resultPointer, Int32(col)))
        let sType : Parameter.Type
        let dbType: ColumnType
        let asString: ((Parameter) -> String)?
        let fromString: ((String) -> Parameter)?
        switch colType {
        case .Int16: sType = Int.self; dbType = .Int16; asString = nil; fromString = nil
        case .Int32: sType = Int.self; dbType = .Int32; asString = nil; fromString = nil
        case .Int64: sType = Int.self; dbType = .Int64; asString = nil; fromString = nil
        case .Boolean: sType = Bool.self; dbType = .Boolean; asString = nil; fromString = nil
 
        case .Timestamp: sType = Date.self; dbType = .Timestamp; asString = nil; fromString = nil
        case .Date: sType = Date.self; dbType = .Date; asString = nil; fromString = nil
        case .SingleFloat: sType = Float.self; dbType = .SingleFloat;  asString = nil; fromString = nil
        case .DoubleFloat: sType = Double.self; dbType = .DoubleFloat;  asString = nil; fromString = nil
        case .String: sType = String.self; dbType = .String;  asString = nil; fromString = nil
        case .UUID: sType = UUID.self; dbType = .UUID;  asString = nil; fromString = nil
        case .Text: sType = String.self; dbType = .String;   asString = nil; fromString = nil
        case .Bytes: sType = Data.self; dbType = .Bytes;   asString = nil; fromString = nil
        }
        return (col: col, name: name, swiftType: sType, dbType: dbType, asString: asString, fromString: fromString)
    }
}
