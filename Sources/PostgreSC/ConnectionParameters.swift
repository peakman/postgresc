#if os(Linux)
    import Glibc
#else
    import Darwin
    import Foundation
#endif

import Utilities

public struct DBServerParameters : Decodable {
    var host: String
    var port: String
    var options: String
    var user: String
    var databaseName: String
    var password: String
}

public struct DBConnections : Decodable {
    var types : [String : DBServerParameters]   // Valid keys are "prod" , "dev" and "hellodev"
}


public struct ConnectionParameters {
    let activeParms : DBServerParameters
    var host : String {return activeParms.host}
    var port : String {return activeParms.port}
    var options : String {return activeParms.options}
    var user : String {
        if activeParms.user == "${USER}" {
            return NSUserName()
        } else {
            return activeParms.user
        }
    }
    var databaseName : String {return activeParms.databaseName}
    var password : String {
        if activeParms.password == "${KEYCHAIN}" {
            guard let pw = findStoredPassword(serviceName: host, accountName: user) else {fatalError("Password not found for user \(user)")}
            return pw
        } else {
            return activeParms.password
        }
    }

    public init(dbType: String, plist: String = "bamford_db_connections.plist") {
        let plReader = PlistReader()
        let parms : DBConnections
        parms = plReader.readPlist(DBConnections.self, name: plist )
        activeParms = parms.types[dbType]!
    }
}
