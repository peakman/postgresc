//
//  PGMetadata.swift
//  Postgres
//
//  Created by Steve Clarke on 01/03/2019.
//  Copyright © 2019 Steve Clarke. All rights reserved.
//

import Foundation

typealias ColumnMetadata = (name: String, index: Int, swiftType: Parameter.Type, dbType: ColumnType, rawType: String?,
    pgNullable: Bool?, swiftNullable: Bool?, swiftDefault: Parameter?, pgDefault: String?)

public class PGMetadata : PsqlRunner , BaseQueryProcessor, ModelGenerator {
    let columnInfo : [ColumnMetadata]
    var infoDict : [String : ColumnMetadata] {return columnInfo.reduce([:]) { (acc,info) in
        var partial = acc
        partial[info.name] = info
        return partial
    }}
    let queryResult: QueryResult?
    let table: String?
    let primaryKey: PrimaryKey?
    let autoIncrements : Bool?
    
    public init(connection: Connection, dbname: String, table: String? = nil,query: Query? = nil, notNull: [BareNotNullSpec]? = nil ) {
        
        func makeMeta(tabInfo: TableInfo?, mfq: MetaFromQuery, notNullSpec: BareNotNullSpec?, index: Int) -> ColumnMetadata {
            let rawType: String?
            let pgNullable: Bool?
            let swiftNullable: Bool?
            let swiftDefault: Parameter?
            let pgDefault: String?
            if let ti = tabInfo {
                rawType = ti.dbType
                pgNullable = ti.nullable
                pgDefault = ti.default
            } else {
                rawType = nil
                pgNullable = nil
                pgDefault = nil
            }
            if let nns = notNullSpec {
                swiftDefault = nns.1
                swiftNullable = false
            } else {
                swiftDefault = nil
                swiftNullable = nil
            }
            let cm : ColumnMetadata = (name: mfq.name, index: index, swiftType: mfq.swiftType, dbType: mfq.dbType, rawType: rawType,
                                       pgNullable: pgNullable, swiftNullable: swiftNullable, swiftDefault: swiftDefault, pgDefault: pgDefault)
            return cm
        }
        
        // MARK:  - Mainline of init starts here
        
        let bQuery: Query
        let tableInfo: [TableInfo]?
        self.table = table
        if query == nil {
            guard table != nil  else {fatalError("Table cannot be nil if query is nil")}
            bQuery =  Query("select * from \(table!)")
        } else {
            bQuery = query!
        }
        if let tabName = table {
            tableInfo = PGMetadata.infoFor(dbname: dbname, table: tabName)
            self.primaryKey = PGMetadata.primaryKeyInfo(dbname: dbname, table: tabName, tableInf: tableInfo)
            self.autoIncrements = PGMetadata.doesAutoIncrement(tabInfo: tableInfo, pKey: self.primaryKey)
        } else {
            tableInfo = nil
            self.primaryKey = nil
            self.autoIncrements = nil
        }
        let qRes = connection.execute(query: bQuery)
        self.queryResult = qRes
        let cols : [Int] = Array(0..<qRes.numberOfColumns)
        self.columnInfo = cols.reduce([]) { (acc, idx) in
            var partial : [ColumnMetadata] = acc
            let mfq = PGMetadata.gatherMetadata(col: idx, qRes: qRes)
            let tirow : TableInfo?
            let notNullSpec: BareNotNullSpec?
            if let notn = notNull {
                notNullSpec = notn.first {$0.0 == mfq.name}
            } else {
                notNullSpec = nil
            }
            if let ti = tableInfo {
                tirow = ti.first {$0.column == mfq.name}
                guard tirow != nil else { fatalError("Could not find table info for \(mfq.name)")}
            } else {
                tirow = nil
            }
            
            let cm = makeMeta(tabInfo: tirow , mfq: mfq, notNullSpec: notNullSpec, index: idx)
            partial.append(cm)
            return partial
        }
        
    }
}
