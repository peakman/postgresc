//
//  PGDecoder.swift
//  Postgres
//
//  Created by Steve Clarke on 28/02/2019.
//  Copyright © 2019 Steve Clarke. All rights reserved.
//

import libpq
import Foundation
import Utilities

public protocol PGEncodable : Encodable  {
    associatedtype P = Parameter
    var nilColumns : [String] {get}
}

typealias PGEncoderResult = (query: Query, values: [Parameter?])

extension PGEncodable {
    public var nilColumns:  [String] {get{
        var nilCols : [String] = []
        let props = Mirror(reflecting: self).children
        for prop in props {
            let pVal =  "\(prop.value)"
            //print("Value is ", prop.value, "Label is ", prop.label)
            guard prop.label != nil else {fatalError("Label should not be nil")}
            if pVal == "nil" {nilCols.append(prop.label!)}
        }
        return nilCols
    }}
    
    public func insert<K,P>(on connection: Connection, using encoder: PGEncoder<K>) -> P {
        do {
            let result : PGEncoderResult = try encoder.encodeQuery(self)
            print("Query is : ", result.query)
            print("Values are : ", result.values)
            let qr = connection.execute(query: result.query, parameters: result.values)
            let row = qr.readResultRowAtIndex(rowIndex: 0)
            guard qr.numberOfRows > 0 else {fatalError("Insert returned zero rows")}
            guard qr.numberOfColumns > 0 else {fatalError("Insert returned zero columns")}
            let rawType : ColumnType? = qr.typesForColumnIndexes[0]
            guard rawType != nil else {fatalError("Unable to get raw type of inserted key")}
            if let inserted = row.columnValues[0] {
                let ret : P
                switch (inserted, P.self) {
                case let (i,p) where i is Int32 && (p == Int?.self || p == Int.self) :
                    ret = Int((i as! Int32 )) as! P
                case let (i,p) where i is Int32 && (p == Int32?.self || p == Int32.self) :
                    ret = i as! Int32 as! P
                case let (i,p) where i is UUID && (p == UUID?.self || p == UUID.self) :
                    ret = i as! UUID as! P
                default: fatalError("Unable to convert database key of type \(rawType!) to \(P.self)")
                }
                return ret
            } else {
                fatalError("Failed to get value of inserted key")
            }
        } catch {
            fatalError("Encoding failed for \(self)")
        }
    }
}

typealias ColumnData = (name: String,  value: Parameter?)


public class PGEncoder<Key>  where Key : PGEncodable {
    typealias K =  CodingKey
    public var codingPath: [CodingKey]
    public var userInfo: [CodingUserInfoKey : Any]
    public var table: String
    public var dbname: String
    public var tableInfo : [TableInfo]
    public var primaryKeyInfo: PrimaryKey
    
    public init( dbname: String, table: String) {
        self.codingPath = []
        self.userInfo = [:]
        self.dbname = dbname
        self.table = table
        self.tableInfo = PGMetadata.infoFor(dbname: dbname, table: table)
        if let pk = PGMetadata.primaryKeyInfo(dbname: dbname, table: table, tableInf: self.tableInfo) {
            self.primaryKeyInfo = pk
        } else {
            fatalError("PGEncoder only supports tables with a primary key")
        }
    }
    
    
    func encodeQuery<T>(_ value: T) throws -> PGEncoderResult where T : PGEncodable {
        var columnsData : [ColumnData]
        let internalEncoder : _PGEncoder
        let values : [Parameter?]
        let sourceColumns : String
        let sourceCount : Int
        func cast(idx: Int) -> String {
            let colname = columnsData.map() {$0.name}[idx]
            let ti = tableInfo.first() {$0.column == colname}
            guard ti != nil else {fatalError("TableInfo not found for \(colname)")}
            return "\(PGMetadata.castFor(info: ti!))"
        }
        var placeHolders : String {
            return (0..<sourceCount).reduce([]) { acc, idx in
                var ret : [String] = acc
                ret.append("$\(idx+1)\(cast(idx: idx))")
                return ret
            }.joined(separator: ",")
        }
        do {
            internalEncoder = _PGEncoder(encoder: self)
            try value.encode(to: internalEncoder)
            columnsData = internalEncoder.columnsData
            for col in value.nilColumns {
                if !primaryKeyInfo.columns.contains(col)   {
                    if (columnsData.first() {$0.name == col}) == nil {
                        columnsData.append((name: col, value: nil))
                    }
                } else {
                    if PGMetadata.doesAutoIncrement(tabInfo: tableInfo, pKey: primaryKeyInfo) == false  {
                        columnsData.append((name: col, value: nil))
                    }
                }
            }
            sourceCount = columnsData.count
            values = columnsData.map() {$0.value }
            sourceColumns = columnsData.map() {$0.name}.joined(separator: ",")
        } catch {
            fatalError("Error encoding \(value): \(error.localizedDescription)")
        }
        let keyCol = primaryKeyInfo.columns[0]
        return (query: Query("insert into \(table) (\(sourceColumns)) values(\(placeHolders)) returning \(keyCol)"), values: values)
    }
    
    internal class _PGEncoder : Encoder {
        let codingPath: [CodingKey] = []
        let userInfo: [CodingUserInfoKey : Any]  = [:]
        let encoder : PGEncoder
        var columnsData : [ColumnData] = []
        var tableInfo : [TableInfo] {return encoder.tableInfo}
        var primaryKeyInfo : PrimaryKey {return encoder.primaryKeyInfo}

        init (encoder: PGEncoder) {
            self.encoder = encoder
        }

        
        public func container<Key>(keyedBy type: Key.Type) -> KeyedEncodingContainer<Key> where Key : CodingKey {
            let kec = KeyedEncodingContainer<Key>(KEC<Key>(codingPath: [],  encoder: self))
            return kec
        }
        
        struct KEC<Key : CodingKey> : KeyedEncodingContainerProtocol {
            
            init(codingPath: [CodingKey],  encoder: _PGEncoder) {
                self.codingPath = codingPath
                self.encoder = encoder
            }
            
            var codingPath: [CodingKey]
            private let encoder : _PGEncoder
            private var tableInfo : [TableInfo] {return encoder.tableInfo}
            private var primaryKeyInfo : PrimaryKey {return encoder.primaryKeyInfo}
            
            func doColumn(name: String, value: Parameter) {
                encoder.columnsData.append((name: name,value: value))
            }
            func infoFor(column: String) -> TableInfo {
                let ti = tableInfo.first(where: {$0.column == column})
                guard ti != nil else {fatalError("No info column \(column)")}
                return ti!
            }
            mutating func encodeNil(forKey key: Key) throws {
                fatalError("Bang")
            }
            
            mutating func encode(_ value: Bool, forKey key: Key) throws {
                doColumn(name: key.stringValue, value: value)
            }
            
            mutating func encode(_ value: String, forKey key: Key) throws {
                doColumn(name: key.stringValue, value: value)
            }
            
            mutating func encode(_ value: Double, forKey key: Key) throws {
                doColumn(name: key.stringValue, value: value)
            }
            
            mutating func encode(_ value: Float, forKey key: Key) throws {
                doColumn(name: key.stringValue, value: value)
            }
            
            mutating func encode(_ value: Int, forKey key: Key) throws {
                let keystr = key.stringValue
                let _ = infoFor(column: keystr) // fatal error if not found
                if !(primaryKeyInfo.columns.contains(keystr) &&
                                    PGMetadata.doesAutoIncrement(tabInfo: tableInfo, pKey: primaryKeyInfo) == true) {
                    let ti = infoFor(column: key.stringValue)
                    let val : Parameter
                    switch ti.type {
                    case .Int16:
                        guard value < Int16.min || value > Int16.max else {fatalError("Value \(value) out of range for column \(keystr)")}
                        val = Int16(exactly: value)!
                    case .Int32: val = Int32(exactly: value)!
                    case .Int64: val = Int64(exactly: value)!
                    default:
                        fatalError("Unable to convert \(value) to type for \(key.stringValue)")
                    }
                    doColumn(name: key.stringValue, value: val)
                }
            }
            
            mutating func encode(_ value: Int8, forKey key: Key) throws {
                let ti = infoFor(column: key.stringValue)
                let val : Parameter
                switch ti.type {
                case .Int16: val = Int16(exactly: value)!
                case .Int32: val = Int32(exactly: value)!
                case .Int64: val = Int64(exactly: value)!
                default:
                    fatalError("Unable to convert \(value) to type for \(key.stringValue)")
                }
                doColumn(name: key.stringValue, value: val)
            }
            
            mutating func encode(_ value: Int16, forKey key: Key) throws {
                let ti = infoFor(column: key.stringValue)
                let val : Parameter
                switch ti.type {
                case .Int16: val = value
                case .Int32: val = Int32(exactly: value)!
                case .Int64: val = Int64(exactly: value)!
                default:
                    fatalError("Unable to convert \(value) to type for \(key.stringValue)")
                }
                doColumn(name: key.stringValue, value: val)
            }
            
            mutating func encode(_ value: Int32, forKey key: Key) throws {
                let keystr = key.stringValue
                let _ = infoFor(column: keystr) // fatal error if not found
                if !(primaryKeyInfo.columns.contains(keystr) &&
                    PGMetadata.doesAutoIncrement(tabInfo: tableInfo, pKey: primaryKeyInfo) == true) {
                    let ti = infoFor(column: key.stringValue)
                    let val : Parameter
                    switch ti.type {
                    case .Int16:
                        guard !(value < Int16.min || value > Int16.max) else {fatalError("Value \(value) out of range for column \(keystr)")}
                        val = Int16(exactly: value)!
                    case .Int32: val = Int32(exactly: value)!
                    case .Int64: val = Int64(exactly: value)!
                    default:
                        fatalError("Unable to convert \(value) to type for \(key.stringValue)")
                    }
                    doColumn(name: key.stringValue, value: val)
                }
            }
            
            mutating func encode(_ value: Int64, forKey key: Key) throws {
                doColumn(name: key.stringValue, value: value)
            }
            
            mutating func encode(_ value: UInt, forKey key: Key) throws {
                let keystr = key.stringValue
                let ti = infoFor(column: keystr)
                let val : Parameter
                switch ti.type {
                case .Int16:
                    guard !(value < Int16.min || value > Int16.max) else {fatalError("Value \(value) out of range for column \(keystr)")}
                    val = Int16(exactly: value)!
                case .Int32:
                    guard !(value < Int32.min || value > Int32.max) else {fatalError("Value \(value) out of range for column \(keystr)")}
                    val = Int32(exactly: value)!
                case .Int64: val = Int64(exactly: value)!
                default:
                    fatalError("Unable to convert \(value) to type for \(key.stringValue)")
                }
                doColumn(name: key.stringValue, value: val)
            }
            
            mutating func encode(_ value: UInt8, forKey key: Key) throws {
                fatalError("UInt8 properties are not supported")
            }
            
            mutating func encode(_ value: UInt16, forKey key: Key) throws {
                fatalError("UInt16 properties are not supported")
            }
            
            mutating func encode(_ value: UInt32, forKey key: Key) throws {
                fatalError("UInt32 properties are not supported")
            }
            
            mutating func encode(_ value: UInt64, forKey key: Key) throws {
                fatalError("UInt64 properties are not supported")
            }
            
            mutating func encode<T>(_ value: T, forKey key: Key) throws where T : Encodable {
                doColumn(name: key.stringValue, value: value as! Parameter)
            }
            
            mutating func nestedContainer<NestedKey>(keyedBy keyType: NestedKey.Type, forKey key: Key) -> KeyedEncodingContainer<NestedKey> where NestedKey : CodingKey {
                fatalError("Bang")
            }
            
            mutating func nestedUnkeyedContainer(forKey key: Key) -> UnkeyedEncodingContainer {
                fatalError("Bang")
            }
            
            mutating func superEncoder() -> Encoder {
                fatalError("Bang")
            }
            
            mutating func superEncoder(forKey key: Key) -> Encoder {
                fatalError("Bang")
            }
            
            
        }
        
        public func unkeyedContainer() -> UnkeyedEncodingContainer {
            fatalError("Bang")
        }
        
        public func singleValueContainer() -> SingleValueEncodingContainer {
            fatalError("Bang")
        }
        
    }

    
}
