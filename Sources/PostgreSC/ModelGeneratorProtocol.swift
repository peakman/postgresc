//
//  ModelGeneratorProtocol.swift
//  PostgreSC
//
//  Created by Steve Clarke on 17/03/2019.
//

import Foundation

protocol ModelGenerator {
    func generateModel(modelType: String, siblingDir: String? ) -> [String]
    var columnInfo : [ColumnMetadata] {get}
    var infoDict : [String : ColumnMetadata] {get}
    var table: String? {get}
    var primaryKey: PrimaryKey? {get}
    var autoIncrements : Bool? {get}

}

extension ModelGenerator {
    var storedProperties : [ColumnMetadata] {return columnInfo.filter() {$0.swiftNullable != nil}}
    public func generateModel(modelType type: String, siblingDir: String? = nil) -> [String] {
        let keyTypes : [ColumnType] = [.Int16, .Int32, .Int64, .UUID]
        let intTypes : [ColumnType] = [.Int16, .Int32, .Int64]
        var source : [String] = ["import Foundation", "import PostgreSC"]
        var saveURL : URL {
            let parentURL = URL(fileURLWithPath: #file).deletingLastPathComponent().deletingLastPathComponent().deletingLastPathComponent().appendingPathComponent("Tests", isDirectory: true)
            let targetURL : URL
            if let sd = siblingDir {
                targetURL = parentURL.deletingLastPathComponent().deletingLastPathComponent().deletingLastPathComponent().appendingPathComponent(sd, isDirectory: true)
            } else {
                targetURL = parentURL.appendingPathComponent("GenerationTests", isDirectory: true)
            }
            return targetURL.appendingPathComponent(type).appendingPathExtension("swift")
        }

        func quotedDefault(parm: Parameter?) -> Parameter? {
            if let p = parm {
                return "\"\(p.asPgString())\""
            } else {
                return nil
            }
        }
        func setInstanceFromCode(name: String )  -> String {
            var source = ""
            if !(primaryKey != nil && (name == primaryKey!.columns[0]) && autoIncrements == true ) {
                source.append("\t\tself.\(name) = \(name)")
            }
            return source
        }
        
        
        func optional(meta: ColumnMetadata) -> Bool {
            return !(meta.swiftNullable == false || meta.pgNullable != false)
        }
        
        func initSignature() -> String {
            var source : String = ""
            for prop in storedProperties {
                let comma = (prop.name == storedProperties.last!.name) ? "" : ","
                let optQ = optional(meta: prop) ? "?" : ""
                //let deflt = (defaults[prop] == nil) ? "" : " = \(sTypes[prop])(\(ctor[prop].0)\(defaults[prop]!))"
                let deflt = ""
                if !(primaryKey != nil && (prop.name == primaryKey!.columns[0]) && autoIncrements == true) {
                    source.append("\(prop.name): \(prop.swiftType)\(optQ)\(deflt)\(comma) ")
                }
            }
            return source
        }
        
        func swiftInfoFrom(meta: ColumnMetadata) -> (sType: Any.Type, deflt: Parameter?) {
            let sType : Any.Type
            let dbType : Any.Type
            let deflt: Parameter?
            let parmDefault : Parameter?
            parmDefault = meta.swiftDefault ?? nil
            switch meta.dbType {
            case .Int16: sType = Int.self; dbType = Int16.self; deflt = parmDefault
            case .Int32: sType = Int.self; dbType = Int32.self; deflt = parmDefault
            case .Int64: sType = Int.self; dbType = Int64.self; deflt = parmDefault
            case .Boolean: sType = Bool.self; dbType = sType; deflt = parmDefault ?? false
            case .Timestamp: sType = Date.self; dbType = sType; deflt = quotedDefault(parm: parmDefault)
            case .Date: sType = Date.self; dbType = sType; deflt = quotedDefault(parm: parmDefault)
            case .SingleFloat: sType = Float.self; dbType = Float32.self; deflt = parmDefault
            case .DoubleFloat: sType = Double.self; dbType = Float64.self; deflt = parmDefault
            case .String: sType = String.self; dbType = sType; deflt = quotedDefault(parm: parmDefault)
            case .UUID: sType = UUID.self; dbType = UUID.self; deflt =  parmDefault
            case .Text: sType = String.self; dbType = sType; deflt = quotedDefault(parm: parmDefault)
            case .Bytes: sType = Data.self; dbType = sType; deflt = parmDefault
            }
            return (sType: sType, deflt: deflt)
        }
        
        func defineProperties(meta: ColumnMetadata) -> String{
            if primaryKey != nil && autoIncrements != nil && meta.name == primaryKey?.columns[0] {
                return ("\tpublic let \(meta.name) : \(swiftInfoFrom(meta: meta).sType)\(autoIncrements! ? "? = nil" : "")  ")
            } else {
                return ("\tpublic let \(meta.name) : \(swiftInfoFrom(meta: meta).sType)\(optional(meta: meta) ? "?" : "")")
            }
        }
        
        func saveSource(_ source: String) {
            print("Generating source")
            do {
                try source.write(to: saveURL, atomically: true, encoding: .utf8)
            } catch {
                fatalError("Unable to write to \(saveURL)")
            }
        }

        

        // MARK: Mainline for source generation starts here
        
        source.append("public final class \(type) : PGEncodable, PGDecodable {")
        for col in storedProperties {
            source.append(defineProperties(meta: col))
        }

        let sigsource = initSignature()
        source.append("\tpublic init(\(sigsource)) {")
        for col in storedProperties.map({$0.name}) {
            source.append(setInstanceFromCode(name: col ))
        }
        source.append("\t}")
        source.append("}\n")
        
        saveSource(source.joined(separator: "\n"))
        return source
    }
}
