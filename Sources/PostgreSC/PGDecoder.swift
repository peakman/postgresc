//
//  PGDecoder.swift
//  Postgres
//
//  Created by Steve Clarke on 28/02/2019.
//  Copyright © 2019 Steve Clarke. All rights reserved.
//

import libpq
import Foundation

public protocol PGCodable : PGEncodable, PGDecodable {}

public protocol PGDecodable : Decodable, CustomDebugStringConvertible  {
    var mirror : Mirror {get}
}

extension PGDecodable   {
    public var mirror : Mirror { get{
        return Mirror.init(reflecting: self)
    }}
    public var debugDescription : String {
        var bits : [String] = []
        for case let (label?, value) in mirror.children {
            bits.append("\(label): \(value)")
        }
        return "\(mirror.subjectType): (\(bits.joined(separator: ",")))"
    }
}

public class PGDecoder<K> : Decoder, IteratorProtocol, Sequence where K : PGDecodable {
    public var codingPath: [CodingKey]
    public var userInfo: [CodingUserInfoKey : Any]
    public var connection: Connection
    public var table: String?
    public var notNull: [BareNotNullSpec]?
    public var dbname: String
    public var numberOfRows : Int { return queryResult.numberOfRows}
    public var currentRow: QueryResultRow? = nil
    public var metadata : PGMetadata
    public var queryResult: QueryResult {
        guard metadata.queryResult != nil else {fatalError("Expected a QueryResult")}
        return metadata.queryResult!
    }
    private var index = 0
    public func next()  -> K? {
        guard index < numberOfRows
            else { return nil }
        do {
            currentRow = queryResult.readResultRowAtIndex(rowIndex: Int32(index))
            let row = try K.init(from: self)
            index += 1
            return row
        } catch {
            fatalError("Expected a row here but got error: \(error.localizedDescription)")
        }
    }
    
    public func resetIteration(startAt: Int = 0) { index = startAt}
    
    public func makeIterator() -> PGDecoder<K> { return self}
    
    public init(connection: Connection, dbname: String, table: String? = nil, query: Query? = nil, notNull: [BareNotNullSpec]? = nil ) {
        self.codingPath = []
        self.userInfo = [:]
        self.connection = connection
        self.notNull = notNull
        self.dbname = dbname
        let realQuery : Query
        if query == nil {
            guard table != nil else {fatalError("Table must be present if query is nil")}
            realQuery = Query("select * from \(table!)")
        } else {
            realQuery = query!
        }
        self.metadata = PGMetadata(connection: connection, dbname: dbname, table: table, query: realQuery, notNull: notNull)
    }
    
    public func container<Key>(keyedBy type: Key.Type) throws -> KeyedDecodingContainer<Key> where Key : CodingKey {
        guard currentRow != nil else {fatalError("Expected a current row")}
        return KeyedDecodingContainer( KDC<Key>(row: currentRow!, meta: metadata))
    }
    
    
    struct KDC<Key : CodingKey> : KeyedDecodingContainerProtocol {
        var row: QueryResultRow
        var meta: PGMetadata
        var codingPath: [CodingKey] = []
        var allKeys: [Key] = []
        
        init(row: QueryResultRow, meta: PGMetadata) {
            self.row = row
            self.meta = meta
        }
        
        func info(forKey key: Key) -> (value: Any, type: ColumnType) {
            let rowMeta = meta.infoDict[key.stringValue]
            guard rowMeta != nil else {fatalError("Expected metadata for \(key.stringValue)")}
            let v : Any
            if let valu = row.columnValues[rowMeta!.index] {
                v = valu
            } else {
                if let valu2 = rowMeta!.swiftDefault {
                    v = valu2
                } else {
                    fatalError("Column \(key.stringValue) is null and no default value is available")
                }
            }
            return (value: v, type:rowMeta!.dbType)
        }
    
        func contains(_ key: Key) -> Bool {
            return meta.infoDict.index(forKey: key.stringValue) != nil
        }
        
        func decodeNil(forKey key: Key) throws -> Bool {
            let rowMeta = meta.infoDict[key.stringValue]
            guard rowMeta != nil else {fatalError("Expected metadata for \(key.stringValue)")}
            return row.columnValues[rowMeta!.index] == nil
        }
        
        func decode(_ type: Bool.Type, forKey key: Key) throws -> Bool {
            return info(forKey: key).value as! Bool
        }
        
        func decode(_ type: String.Type, forKey key: Key) throws -> String {
            return info(forKey: key).value as! String
        }
        
        func decode(_ type: Double.Type, forKey key: Key) throws -> Double {
            return info(forKey: key).value as! Double
        }
        
        func decode(_ type: Float.Type, forKey key: Key) throws -> Float {
            return info(forKey: key).value as! Float
        }
        
        func decode(_ type: Int.Type, forKey key: Key) throws -> Int {
            let inf = info(forKey: key)
            let v : Int
            switch inf.type {
            case .Int16: v = Int(inf.value as! Int16)
            case .Int32: v = Int(inf.value as! Int32)
            case .Int64: v = Int(inf.value as! Int64)
            default: fatalError("Unexpected type \(type) from database")
            }
            return v
        }
        
        func decode(_ type: Int8.Type, forKey key: Key) throws -> Int8 {
            return info(forKey: key).value as! Int8
        }
        
        func decode(_ type: Int16.Type, forKey key: Key) throws -> Int16 {
            return info(forKey: key).value as! Int16
        }

        func decode(_ type: Int32.Type, forKey key: Key) throws -> Int32 {
            return info(forKey: key).value as! Int32
        }
        
        func decode(_ type: Int64.Type, forKey key: Key) throws -> Int64 {
            return info(forKey: key).value as! Int64
        }
        
        func decode(_ type: UInt.Type, forKey key: Key) throws -> UInt {
            let dbinf =  info(forKey: key)
            let v : UInt
            let dbv = dbinf.value
            switch dbv {
            case is UInt:
                v = dbv as! UInt
            case is Int16:
                let i16 = dbv as! Int16
                guard i16 >= 0  else {fatalError("Cannot convert negative number \(i16) to UInt")}
                v = UInt(i16)
            case is Int32:
                let i32 = dbv as! Int32
                guard i32 >= 0  else {fatalError("Cannot convert negative number \(i32) to UInt")}
                v = UInt(i32)
            default: fatalError("Cannot convert value of type \(dbinf.type) to UInt")
            }
            return v
        }
        
        func decode(_ type: UInt8.Type, forKey key: Key) throws -> UInt8 {
            return info(forKey: key).value as! UInt8
        }
        
        func decode(_ type: UInt16.Type, forKey key: Key) throws -> UInt16 {
            return info(forKey: key).value as! UInt16
        }
        
        func decode(_ type: UInt32.Type, forKey key: Key) throws -> UInt32 {
            return info(forKey: key).value as! UInt32
        }
        
        func decode(_ type: UInt64.Type, forKey key: Key) throws -> UInt64 {
            return info(forKey: key).value as! UInt64
        }
        
        func decode<T>(_ type: T.Type, forKey key: Key) throws -> T where T : Decodable {
            let v : T
            switch type {
            case is Date.Type:
                v =  info(forKey: key).value as! T
            case is UUID.Type:
                v =  info(forKey: key).value as! T
            default: fatalError("Unexpected type \(type) here")
            }
            return v
        }
        
        func nestedContainer<NestedKey>(keyedBy type: NestedKey.Type, forKey key: Key) throws -> KeyedDecodingContainer<NestedKey> where NestedKey : CodingKey {
            fatalError()
        }
        
        func nestedUnkeyedContainer(forKey key: Key) throws -> UnkeyedDecodingContainer {
            fatalError()
        }
        
        func superDecoder() throws -> Decoder {
            fatalError()
        }
        
        func superDecoder(forKey key: Key) throws -> Decoder {
            fatalError()
        }
        
        
        
    }
    
    public func unkeyedContainer() throws -> UnkeyedDecodingContainer {
        print("Called unkeyedContainer")
        fatalError()
    }
    
    public func singleValueContainer() throws -> SingleValueDecodingContainer {
        print("Called singleValueContainer")
        fatalError()
    }
}
