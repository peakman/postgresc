//
//  PsqlRunner.swift
//  Postgres
//
//  Created by Steve Clarke on 01/03/2019.
//  Copyright © 2019 Steve Clarke. All rights reserved.
//

import Foundation
public typealias BareNotNullSpec = (String, Parameter?)   // Column name and default value
public typealias PrimaryKey  = (columns: [String], pgTypes: [ColumnType])
public typealias TableInfo = (column: String, type: ColumnType , nullable: Bool, default: String?, dbType: String)

public protocol PsqlRunner {
    static func runPsql(command: String, options: [String]? , dbname: String) -> [String]
    static func primaryKeyInfo(dbname: String, table: String, tableInf: [TableInfo]? ) -> PrimaryKey?
    static func tableInfo(from attr: String) -> TableInfo
    static func doesAutoIncrement(tabInfo: [TableInfo]?, pKey: PrimaryKey?) -> Bool?
    static func infoFor(dbname: String, table: String) -> [TableInfo] 
    
}
extension PsqlRunner {
    public static func runPsql(command: String, options: [String]? = ["-t", "-A"], dbname: String = "bamford_eu") -> [String] {
        let pipe = Pipe()
        let process = Process()
        var arguments = [ "-F=|", "-c", command,  "-U", NSUserName() , dbname]
        if let opts = options {
            for opt in opts {arguments.insert(opt, at: 0)}
        }
        process.arguments = arguments
        process.launchPath = "/usr/local/bin/psql"
        process.standardOutput = pipe
        process.launch()
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        process.waitUntilExit()
        guard process.terminationStatus == 0 else { fatalError( "psql termination status is \(process.terminationStatus)") }
        let output = String(data: data, encoding: .utf8)!
        return String(output.dropLast()).split(separator: "\n").map {String($0)}
    }
    public static func primaryKeyInfo(dbname: String, table: String, tableInf: [TableInfo]? = nil) -> PrimaryKey? {
        let command = "\\d \(table)"
        let output = runPsql(command: command,options:  ["-A"], dbname: dbname)
        let keyLine = output.first() { $0.contains("PRIMARY KEY")}
        guard keyLine != nil else { return nil}
        let pattern = "^.+PRIMARY KEY[^(]+\\(([^)]+)\\)"
        let re = try! NSRegularExpression(pattern: pattern, options: [])
        let chkRes = re.firstMatch(in: keyLine!, options: [], range: NSMakeRange(0, keyLine!.utf8.count ))
        let rng = chkRes?.range(at: 1)
        let colNames =  String(keyLine!.utf8.dropLast(keyLine!.utf8.count - rng!.location - rng!.length).dropFirst(rng!.location))!
        let tableInfo = tableInf ?? infoFor(dbname: dbname, table: table)
        let keyCols  = colNames.split(separator: ",").map() {String($0).replacingOccurrences(of: " ", with: "")}
        let pgTypes : [ColumnType] = keyCols.reduce([]) { (mem, name) in
            var newMem = mem
            let infoRow = tableInfo.first() {$0.column == name}
            guard infoRow != nil else {
                print("newMem: ", newMem, ", keyCols: ", keyCols)
                fatalError("Cannot find row for key \(name)")
            }
            newMem.append(infoRow!.type)
            return newMem
        }
        return (columns: keyCols , pgTypes: pgTypes)
    }
    
    public static func tableInfo(from attr: String) -> TableInfo {
        let fields = attr.split(separator:  "|").map {$0.dropLast()}
        let column : String = String(fields[0])
        let type: ColumnType = ColumnType.fromPsql(string: String(fields[1]))
        //ignore collation
        let boolString = String(fields[3])
        let nullable : Bool
        if boolString.utf8.count == 0 {
            nullable = true
        } else {
            nullable = (boolString != "not null")
        }
        let defltString = String(fields[4])
        let deflt : String?
        if defltString.utf8.count == 0 {deflt = nil} else {deflt = defltString}
        return (column: column, type: type , nullable: nullable, default: deflt, dbType: String(fields[1]))
    }
    
    public static func infoFor(dbname: String, table: String) -> [TableInfo] {
        let info = runPsql(command: "\\d+ \(table)", dbname: dbname)
        return info.map { tableInfo(from: $0)}
    }
        
    public static func doesAutoIncrement(tabInfo: [TableInfo]?, pKey: PrimaryKey?) -> Bool? {
        if let primaryKey = pKey {
            if let tableInfo = tabInfo {
                let keyInfo : TableInfo? = tableInfo.first() { $0.column == primaryKey.columns[0]}
                guard keyInfo != nil else {fatalError("No info for primary key \(primaryKey.columns[0])")}
                if let def = keyInfo!.default {
                    return def.hasPrefix("nextval")  || def.hasPrefix("generated always")
                } else {
                    return false
                }
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    public static func castFor(info: TableInfo) -> String {
        // returns the text required to follow a placeholder in a query e.g $2::smallint
        let cast : String?
        switch info.type {
        case .String: cast = nil
        case .Int32: cast = "int"
        default: cast = info.dbType
        }
        if cast != nil {
            return "::\(cast!)"
        } else {
            return ""
        }
    }

}

