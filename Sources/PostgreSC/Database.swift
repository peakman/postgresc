
import libpq

public enum PgError: Error {
    public typealias RawValue = String
    
    case ConnectionFailed(message: String)
    case Unclassified(message: String)
}

public class Database {
    public static func connect(dbType: String = "dev")  -> Connection {
            return self.reallyConnect(parameters: ConnectionParameters(dbType: dbType))
    }
    public static func reallyConnect(parameters: ConnectionParameters = ConnectionParameters(dbType: "dev")) -> Connection {

        let connectionPointer : OpaquePointer
        do {
            connectionPointer = PQsetdbLogin(   parameters.host,
                                                parameters.port,
                                                parameters.options,
                                                "",
                                                parameters.databaseName,
                                                parameters.user,
                                                parameters.password)
            guard PQstatus(connectionPointer) == CONNECTION_OK else {
                
                let message = String(cString: PQerrorMessage(connectionPointer))
                fatalError("Connection failed: \(message)" )
            }
        }

        return Connection(pointer: connectionPointer)
    }
}
