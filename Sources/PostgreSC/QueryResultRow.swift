import Foundation
public struct QueryResultRow {
    public let columnValues: [Any?]
    unowned let queryResult: QueryResult

    public subscript(columnName: String) -> Any? {
        get {
            guard let index = queryResult.columnIndexesForNames[columnName] else { return nil }
            return columnValues[index]
        }
    }
    public func indexForCol(_ name: String) -> Int {
        guard let index =  queryResult.columnIndexesForNames[name] else { fatalError("No such column \(name)") }
        return index
    }
    public func dateForCol(_ name: String) -> Date? {
        return columnValues[indexForCol(name)] as! Date?
    }
    public func stringForCol(_ name: String) -> String? {
        return columnValues[indexForCol(name)] as! String?
    }
}
