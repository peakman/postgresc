//
//  TypedRow.swift
//  Postgres
//
//  Created by Steve Clarke on 26/02/2019.
//  Copyright © 2019 Steve Clarke. All rights reserved.
//

import libpq
import Foundation

public protocol ValidKeyType  {
    associatedtype K
    var defaultInsertKey : K {get}
    var type : Parameter.Type {get}
}

extension ValidKeyType {
    public var defaultInsertKey: Int {get{return 0}}
    public var type: Parameter.Type {fatalError("Cannot get column type for generic ValidKeyType")}
    
}

extension Optional : ValidKeyType {
    public var defaultInsertKey : Int? {get{ return nil}}
}

extension Int : ValidKeyType {
    public var defaultInsertKey: Int {get{return 0}}
    public var type: Parameter.Type {return Int.self}
}
extension UUID : ValidKeyType {
    public  var defaultInsertKey : UUID {get{ return UUID()}}
    public var type: Parameter.Type {return UUID.self}
}

